package main

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/cosmos/cosmos-sdk/codec"
	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	coretypes "github.com/tendermint/tendermint/rpc/core/types"
	"github.com/urfave/cli/v2"
	ctypes "gitlab.com/thorchain/binance-sdk/common/types"
	"gitlab.com/thorchain/binance-sdk/keys"
	ttypes "gitlab.com/thorchain/binance-sdk/types"
	"gitlab.com/thorchain/binance-sdk/types/msg"
	btx "gitlab.com/thorchain/binance-sdk/types/tx"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
)

// GetAccountByAddress get an account information by it's address
func GetAccountByAddress(binanceURL string, accountAddr string) (common.Account, error) {
	u, err := url.Parse(binanceURL)
	if err != nil {
		return common.Account{}, err
	}
	u.Path = "/abci_query"
	v := u.Query()
	v.Set("path", fmt.Sprintf("\"/account/%s\"", accountAddr))
	u.RawQuery = v.Encode()

	resp, err := http.Get(u.String())
	if err != nil {
		return common.Account{}, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Println("fail to close response body ", err)
		}
	}()

	type queryResult struct {
		Jsonrpc string `json:"jsonrpc"`
		ID      string `json:"id"`
		Result  struct {
			Response struct {
				Key         string `json:"key"`
				Value       string `json:"value"`
				BlockHeight string `json:"height"`
			} `json:"response"`
		} `json:"result"`
	}

	var result queryResult
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return common.Account{}, err
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return common.Account{}, err
	}

	data, err := base64.StdEncoding.DecodeString(result.Result.Response.Value)
	if err != nil {
		return common.Account{}, err
	}

	cdc := ttypes.NewCodec()
	var acc ctypes.AppAccount
	err = cdc.UnmarshalBinaryBare(data, &acc)
	if err != nil {
		return common.Account{}, err
	}
	coins, err := common.GetCoins(common.BNBChain, acc.BaseAccount.Coins)
	if err != nil {
		return common.Account{}, fmt.Errorf("fail to convert coins: %w", err)
	}
	account := common.NewAccount(acc.BaseAccount.Sequence, acc.BaseAccount.AccountNumber, coins, acc.Flags > 0)

	return account, nil
}
func getChainID(ctx *cli.Context, binanceURL string) (string, error) {
	u, err := url.Parse(binanceURL)
	if err != nil {
		return "", fmt.Errorf("unable to parse rpc host: %s: %w", binanceURL, err)
	}

	u.Path = "/status"

	resp, err := http.Get(u.String())
	if err != nil {
		return "", fmt.Errorf("fail to get response from %s/status, err:%w", binanceURL, err)
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Println("fail to close resp body")
		}
	}()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("fail to read response body,err: %w", err)
	}

	type Status struct {
		Jsonrpc string `json:"jsonrpc"`
		ID      string `json:"id"`
		Result  struct {
			NodeInfo struct {
				Network string `json:"network"`
			} `json:"node_info"`
		} `json:"result"`
	}

	var status Status
	if err := json.Unmarshal(data, &status); err != nil {
		return "", fmt.Errorf("fail to unmarshal body: %w", err)
	}

	return status.Result.NodeInfo.Network, nil
}
func sendBinanceTransaction(ctx *cli.Context, binanceURL, key, to, memo string, account common.Account, fromAddress string) error {
	km, err := keys.NewPrivateKeyManager(key)
	if err != nil {
		return fmt.Errorf("fail to create private key manager, err: %w", err)
	}
	chainID, err := getChainID(ctx, binanceURL)
	if err != nil {
		return fmt.Errorf("fail to get chain id,err:%w", err)
	}
	if strings.EqualFold(chainID, "Binance-Chain-Ganges") {
		ctypes.Network = ctypes.TestNetwork
	}
	toAddr, err := ctypes.AccAddressFromBech32(to)
	if err != nil {
		return fmt.Errorf("fail to decode account address,err: %w", err)
	}
	var payload []msg.Transfer
	var coins ctypes.Coins
	gas := 6000 * len(account.Coins)
	network := common.GetCurrentChainNetwork()
	switch network {
	case common.MockNet:
		gas = 30000 * len(account.Coins)
	}

	for _, coin := range account.Coins {
		if coin.Asset.Equals(common.BNBAsset) {
			coin.Amount = coin.Amount.SubUint64(uint64(gas))
		}
		coins = append(coins, ctypes.Coin{
			Denom:  coin.Asset.Symbol.String(),
			Amount: int64(coin.Amount.Uint64()),
		})
	}

	payload = append(payload, msg.Transfer{
		ToAddr: toAddr,
		Coins:  coins,
	})
	sendMsg, err := parseTx(fromAddress, payload)
	if err != nil {
		return fmt.Errorf("fail to create send message, err:%w", err)
	}
	if err := sendMsg.ValidateBasic(); err != nil {
		return fmt.Errorf("invalid send msg: %w", err)
	}

	signMsg := btx.StdSignMsg{
		ChainID:       chainID,
		Memo:          memo,
		Msgs:          []msg.Msg{sendMsg},
		Source:        btx.Source,
		Sequence:      account.Sequence,
		AccountNumber: account.AccountNumber,
	}
	result, err := km.Sign(signMsg)
	if err != nil {
		return fmt.Errorf("fail to sign tx,err: %w", err)
	}
	u, err := url.Parse(binanceURL)
	if err != nil {
		return fmt.Errorf("fail to parse %s,err: %w", binanceURL, err)
	}
	u.Path = "broadcast_tx_commit"
	values := u.Query()
	values.Set("tx", "0x"+hex.EncodeToString(result))
	u.RawQuery = values.Encode()
	resp, err := http.Post(u.String(), "", nil)
	if err != nil {
		return fmt.Errorf("fail to broadcast tx to binance chain: %w", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("fail to read response body: %w", err)
	}
	type BroadcastResult struct {
		JSONRPC string                            `json:"jsonrpc"`
		Result  coretypes.ResultBroadcastTxCommit `json:"result"`
	}
	var commit BroadcastResult
	cdc := MakeLegacyCodec()
	err = cdc.UnmarshalJSON(body, &commit)
	if err != nil {
		return fmt.Errorf("fail to unmarshal commit: %w", err)
	}
	fmt.Println("body:", string(body))
	fmt.Println("binance tx: ", commit.Result.Hash.String())
	return nil
}
func MakeLegacyCodec() *codec.LegacyAmino {
	cdc := codec.NewLegacyAmino()
	banktypes.RegisterLegacyAminoCodec(cdc)
	authtypes.RegisterLegacyAminoCodec(cdc)
	cosmos.RegisterCodec(cdc)
	return cdc
}
func parseTx(fromAddr string, transfers []msg.Transfer) (msg.SendMsg, error) {
	addr, err := ctypes.AccAddressFromBech32(fromAddr)
	if err != nil {
		return msg.SendMsg{}, fmt.Errorf("fail to decode from address,err: %w", err)
	}
	fromCoins := ctypes.Coins{}
	for _, t := range transfers {
		t.Coins = t.Coins.Sort()
		fromCoins = fromCoins.Plus(t.Coins)
	}
	return createMsg(addr, fromCoins, transfers), nil
}
func createMsg(from ctypes.AccAddress, fromCoins ctypes.Coins, transfers []msg.Transfer) msg.SendMsg {
	input := getInput(from, fromCoins)
	output := make([]msg.Output, 0, len(transfers))
	for _, t := range transfers {
		t.Coins = t.Coins.Sort()
		output = append(output, getOutput(t.ToAddr, t.Coins))
	}
	return msgToSend([]msg.Input{input}, output)
}
func msgToSend(in []msg.Input, out []msg.Output) msg.SendMsg {
	return msg.SendMsg{Inputs: in, Outputs: out}
}
func getInput(addr ctypes.AccAddress, coins ctypes.Coins) msg.Input {
	return msg.Input{
		Address: addr,
		Coins:   coins,
	}
}

func getOutput(addr ctypes.AccAddress, coins ctypes.Coins) msg.Output {
	return msg.Output{
		Address: addr,
		Coins:   coins,
	}
}
