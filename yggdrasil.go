package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getYggdrasilByPubKey(serverIP string, port int, pubKey string) (types.Vault, error) {
	buf, err := getFromThorchain(serverIP, port, "vault/"+pubKey)
	if err != nil {
		return types.Vault{}, fmt.Errorf("fail to get yggdrasil vaults from thorchain: %w", err)
	}
	var yggdrasilVaultsResp types.QueryVaultResp
	if err := json.Unmarshal(buf, &yggdrasilVaultsResp); err != nil {
		return types.Vault{}, fmt.Errorf("fail to unmarshal yggdrasil vaults")
	}

	return types.Vault{
		BlockHeight:           yggdrasilVaultsResp.BlockHeight,
		PubKey:                yggdrasilVaultsResp.PubKey,
		Coins:                 yggdrasilVaultsResp.Coins,
		Type:                  yggdrasilVaultsResp.Type,
		Status:                yggdrasilVaultsResp.Status,
		StatusSince:           yggdrasilVaultsResp.StatusSince,
		Membership:            yggdrasilVaultsResp.Membership,
		Chains:                yggdrasilVaultsResp.Chains,
		InboundTxCount:        yggdrasilVaultsResp.InboundTxCount,
		OutboundTxCount:       yggdrasilVaultsResp.OutboundTxCount,
		PendingTxBlockHeights: yggdrasilVaultsResp.PendingTxBlockHeights,
		Routers:               yggdrasilVaultsResp.Routers,
	}, nil
}
func getYggdrasil(serverIP string, port int) (types.Vaults, error) {
	buf, err := getFromThorchain(serverIP, port, "vaults/yggdrasil")
	if err != nil {
		return nil, fmt.Errorf("fail to get yggdrasil vaults from thorchain: %w", err)
	}
	var yggdrasilVaultsResp []types.QueryYggdrasilVaults
	if err := json.Unmarshal(buf, &yggdrasilVaultsResp); err != nil {
		return nil, fmt.Errorf("fail to unmarshal yggdrasil vaults")
	}
	var resp types.Vaults
	for _, item := range yggdrasilVaultsResp {
		resp = append(resp, types.Vault{
			BlockHeight:           item.BlockHeight,
			PubKey:                item.PubKey,
			Coins:                 item.Coins,
			Type:                  item.Type,
			Status:                types.VaultStatus_ActiveVault,
			StatusSince:           item.StatusSince,
			Membership:            item.Membership,
			Chains:                item.Chains,
			InboundTxCount:        item.InboundTxCount,
			OutboundTxCount:       item.OutboundTxCount,
			PendingTxBlockHeights: item.PendingTxBlockHeights,
			Routers:               item.Routers,
		})
	}
	return resp, nil
}

func getFromThorchain(serverIP string, port int, path string) ([]byte, error) {
	url := fmt.Sprintf("http://%s:%d/thorchain/%s", serverIP, port, path)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("fail to call %s :%w", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	return body, nil
}

func printYggdrasilVault(ctx *cli.Context, vault types.Vault, account common.Account) error {
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetBorder(true)
	tw.SetRowLine(true)
	tw.SetHeader([]string{
		"item",
		"value",
	})
	tw.Append([]string{
		"status",
		vault.Status.String(),
	})
	thornodeAddr, err := vault.PubKey.GetAddress(common.THORChain)
	if err != nil {
		return fmt.Errorf("fail to get thornode address: %w", err)
	}
	tw.Append([]string{
		"THORNODE Address:",
		thornodeAddr.String(),
	})
	bnbAddr, err := vault.PubKey.GetAddress(common.BNBChain)
	if err != nil {
		return fmt.Errorf("fail to get binance address: %w", err)
	}
	tw.Append([]string{
		"BNB Address",
		bnbAddr.String(),
	})

	btcAddr, err := vault.PubKey.GetAddress(common.BTCChain)
	if err != nil {
		return fmt.Errorf("fail to get BTC address: %w", err)
	}
	tw.Append([]string{
		"BTC Address",
		btcAddr.String(),
	})
	totalBTC, err := getBTCAddressBalance(ctx, btcAddr.String())
	if err != nil {
		return fmt.Errorf("fail to get BTC balance: %w", err)
	}
	account.Coins = append(account.Coins, common.NewCoin(common.BTCAsset, cosmos.NewUint(uint64(totalBTC*float64(common.One)))))
	bchAddr, err := vault.PubKey.GetAddress(common.BCHChain)
	if err != nil {
		return fmt.Errorf("fail to get BCH address: %w", err)
	}
	tw.Append([]string{
		"BCH Address",
		bchAddr.String(),
	})
	totalBch, err := getBCHAddressBalance(ctx, bchAddr.String())
	if err != nil {
		return fmt.Errorf("fail to get BCH address balance: %w", err)
	}

	account.Coins = append(account.Coins, common.NewCoin(common.BCHAsset, cosmos.NewUint(uint64(totalBch*float64(common.One)))))
	ltcAddr, err := vault.PubKey.GetAddress(common.LTCChain)
	if err != nil {
		return fmt.Errorf("fail to get BCH address: %w", err)
	}
	tw.Append([]string{
		"LTC Address",
		ltcAddr.String(),
	})
	totalLTC, err := getLTCAddressBalance(ctx, ltcAddr.String())
	if err != nil {
		return fmt.Errorf("fail to get LTC balance: %w", err)
	}
	account.Coins = append(account.Coins, common.NewCoin(common.LTCAsset, cosmos.NewUint(uint64(totalLTC*float64(common.One)))))

	ethAddr, err := vault.PubKey.GetAddress(common.ETHChain)
	if err != nil {
		return fmt.Errorf("fail to get ETH address: %w", err)
	}
	tw.Append([]string{
		"ETH Address",
		ethAddr.String(),
	})
	var ethContract types.ChainContract
	for _, r := range vault.Routers {
		if r.Chain.Equals(common.ETHChain) {
			ethContract = r
			break
		}
	}
	tw.Append([]string{
		"ETH Contract",
		ethContract.Router.String(),
	})
	if !ethContract.IsEmpty() {
		for _, item := range vault.Coins {
			if !item.Asset.Chain.Equals(common.ETHChain) {
				continue
			}

			if item.Asset.Equals(common.ETHAsset) {
				balance, err := getETHAddressBalance(ctx, ethAddr.String(), ethContract.Router.String(), ETHToken)
				if err != nil {
					return fmt.Errorf("fail to get ETH balance: %w", err)
				}
				account.Coins = append(account.Coins, common.NewCoin(item.Asset, cosmos.NewUint(balance)))
				continue
			}
			parts := strings.Split(item.Asset.Symbol.String(), "-")
			balance, err := getETHAddressBalance(ctx, ethAddr.String(), ethContract.Router.String(), parts[1])
			if err != nil {
				return fmt.Errorf("fail to get ETH balance: %w", err)
			}
			account.Coins = append(account.Coins, common.NewCoin(item.Asset, cosmos.NewUint(balance)))
		}
	}
	tw.Render()
	tw = tablewriter.NewWriter(os.Stdout)
	tw.SetBorder(true)
	tw.SetRowLine(true)
	tw.SetHeader([]string{
		"coin",
		"yggdrasil",
		"wallet",
		"diff",
		"status",
	})
	for _, coin := range vault.Coins {
		walletCoin := account.Coins.GetCoin(coin.Asset)
		status := aurora.Green("OK")
		if !walletCoin.Amount.Equal(coin.Amount) {
			status = aurora.Red("FAIL")
		}
		tw.Append([]string{
			coin.Asset.String(),
			getHumanReadableAmount(coin.Amount),
			getHumanReadableAmount(walletCoin.Amount),
			getDiff(walletCoin.Amount, coin.Amount),
			status.String(),
		})
	}
	tw.Render()
	return nil
}
func getHumanReadableAmount(input cosmos.Uint) string {
	return strconv.FormatFloat(float64(input.Uint64())/float64(common.One), 'f', -1, 64)
}
func getDiff(first, second cosmos.Uint) string {
	if first.Equal(second) {
		return "0"
	}
	if first.GT(second) {
		return fmt.Sprintf("+%s", getForDisplay(first.Sub(second)))
	}
	return fmt.Sprintf("-%s", getForDisplay(second.Sub(first)))
}
func getForDisplay(input cosmos.Uint) string {
	return strconv.FormatFloat(float64(input.Uint64())/float64(common.One), 'f', -1, 64)
}
