On THORChain , when a node join the network and become active , the network will fund the node with all kinds of assets , it is called yggdrasil vault , and when the node get churn out , all yggdrasil vault should be returned.

If a node get into trouble , and fail to return yggdrasil fund , then the node won't be able to unbond , and won't be able to leave and get bond back. 

If a node become `Disabled` and failed to return yggdrasil fund , it will be slashed bond.  1.5 x the asset valut in the yggdrasil vault.

This tool allow you to manually return the yggdrasil fund.

It is not very user friendly ,

## Prerequisite

In order to use this cli tool to return yggdrasil fund , you need to make sure you meet the following requirements.
1) You have BTC/BCH/LTC/ETH/BNB chain running , and you can access it's RPC endpoint. If your node is active / or standby , you should have it.

## How to get my hex encoded private key

https://gitlab.com/thorchain/thornode/-/wikis/How-to-get-your-private-key-using-your-mnemonic


## How to build this tool
This tool is written in Go , so if you want to build it yourself , make sure you have Go setup correctly

### Build
```bash
CGO_ENABLED=0 go build
```

## Run it locally
In order to run it locally , you need to use port forward to your BTC/BCH/LTC/BNB nodes

```shell
# bitcoin cash
kubectl -n thornode-testnet port-forward service/bitcoin-cash-daemon 28443:18332
# ETH
kubectl -n thornode-testnet port-forward service/ethereum-daemon 8545:8545
# bitcoin
kubectl -n thornode-testnet port-forward service/bitcoin-daemon 18443:18332
# LTC
kubectl -n thornode-testnet port-forward service/litecoin-daemon 38443:19332
```
Binance has some public seed nodes , we can use that for this purpose.

testnet:  http://data-seed-pre-2-s1.binance.org/

mainnet:  https://dataseed1.defibit.io/

### Testnet
```shell
# you can use any thornode
export THORNODE_IP=your thornode ip address
export BTC_HOST=btc node ip and port
export BCH_HOST=
export LTC_HOST=
export ETH_HOST=
export BINANCE_URL=
NET=testnet ./manual-yggdrasil-return --key "hex encoded private key" 
```

### Mainnet
```shell
export THORNODE_IP=your thornode ip address
export BTC_HOST=btc node ip and port
export BCH_HOST=
export LTC_HOST=
export ETH_HOST=
export BINANCE_URL=
./manual-yggdrasil-return --key "hex encoded private key" --btc-network mainnet --bch-network mainnet --ltc-network mainnet
```

## Run it in bifrost container
First of all  get into bifrost shell
```
make shell 
choose bifrost
```

