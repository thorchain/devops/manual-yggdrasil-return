package main

import (
	"encoding/hex"
	"errors"
	"fmt"

	"github.com/ltcsuite/ltcd/btcec"
	"github.com/ltcsuite/ltcd/btcjson"
	"github.com/ltcsuite/ltcd/chaincfg"
	"github.com/ltcsuite/ltcd/chaincfg/chainhash"
	"github.com/ltcsuite/ltcd/rpcclient"
	"github.com/ltcsuite/ltcd/wire"
	"github.com/ltcsuite/ltcutil"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"github.com/urfave/cli/v2"
	txscript "gitlab.com/thorchain/bifrost/ltcd-txscript"
)

var (
	// LTC settings
	ltcHost            string
	ltcRpcUserName     string
	ltcRpcUserPassword string
)

func getLitecoinClient() *rpcclient.Client {
	client, err := rpcclient.New(&rpcclient.ConnConfig{
		Host:         ltcHost,
		User:         ltcRpcUserName,
		Pass:         ltcRpcUserPassword,
		DisableTLS:   true,
		HTTPPostMode: true,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}
func getLitecoinNetworkConfig(ctx *cli.Context) *chaincfg.Params {
	net := ctx.String("ltc-network")
	switch net {
	case chaincfg.RegressionNetParams.Name:
		return &chaincfg.RegressionNetParams
	case chaincfg.MainNetParams.Name:
		return &chaincfg.MainNetParams
	case chaincfg.TestNet4Params.Name:
		return &chaincfg.TestNet4Params
	}
	return nil
}
func getLTCAddressBalance(ctx *cli.Context, address string) (float64, error) {
	client := getLitecoinClient()
	if len(address) == 0 {
		return 0.0, errors.New("please provide address")
	}
	addr, err := ltcutil.DecodeAddress(address, getLitecoinNetworkConfig(ctx))
	if err != nil {
		panic(err)
	}
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []ltcutil.Address{
		addr,
	})
	if err != nil {
		return 0.0, fmt.Errorf("fail to list account: %w", err)
	}
	if ctx.Bool("verbose") {
		fmt.Println("LTC address:", address, "utxos:", len(result))
	}
	var total float64
	for _, item := range result {
		total += item.Amount
	}
	return total, nil
}
func getLitecoinAddressFromPrivateKey(ctx *cli.Context, priKey *btcec.PrivateKey) (string, error) {
	pubKeyBuf := priKey.PubKey().SerializeCompressed()
	secpPubKey := secp256k1.PubKey(pubKeyBuf)
	addr, err := ltcutil.NewAddressWitnessPubKeyHash(secpPubKey.Address().Bytes(), getLitecoinNetworkConfig(ctx))
	if err != nil {
		return "", fmt.Errorf("fail to create address: %w", err)
	}
	return addr.EncodeAddress(), nil
}
func getLitecoinUTXO(from ltcutil.Address) ([]btcjson.ListUnspentResult, error) {
	client := getLitecoinClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []ltcutil.Address{
		from,
	})
	if err != nil {
		return nil, fmt.Errorf("fail to get unspent utox: %w", err)
	}
	var utxo []btcjson.ListUnspentResult
	for _, item := range result {
		if item.Amount == 0 {
			continue
		}
		if item.Confirmations == 0 && item.Amount < 10000 {
			continue
		}
		utxo = append(utxo, item)
	}
	return utxo, nil
}

// estimateLTCTxSize will create a temporary MsgTx, and use it to estimate the final tx size
// the value in the temporary MsgTx is not real
// https://bitcoinops.org/en/tools/calc-size/
func estimateLTCTxSize(memo string, txes []btcjson.ListUnspentResult) int64 {
	// overhead - 10.75
	// Per Input - 67.75
	// Per output - 31 , we sometimes have 2 output , and sometimes only have 1 , it depends ,here we only count 1
	// it is better to underestimate rather than over estimate
	// 10.5 overhead for null data
	// len(memo) is the size of memo put in null data
	// these get us very close to the final vbytes.
	// multiple by 100 , and then add, so don't need to deal with float
	return int64((1075+6775*len(txes)+1050)/100) + int64(31+len([]byte(memo)))
}
func sendLitecoinAction(ctx *cli.Context, key, to string, gasRate int64, memo string) error {
	client := getLitecoinClient()
	fromPriKey, err := hex.DecodeString(key)
	if err != nil {
		return fmt.Errorf("fail to decode from private key: %w", err)
	}
	pkey, _ := btcec.PrivKeyFromBytes(btcec.S256(), fromPriKey)
	fromAddr, err := getLitecoinAddressFromPrivateKey(ctx, pkey)
	if err != nil {
		return fmt.Errorf("fail to get address from private key: %w", err)
	}
	fromAddress, err := ltcutil.DecodeAddress(fromAddr, getLitecoinNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode from address(%s): %w", fromAddr, err)
	}
	payToSource, err := txscript.PayToAddrScript(fromAddress)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	toAddr, err := ltcutil.DecodeAddress(to, getLitecoinNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode address(%s): %w", to, err)
	}

	// add a little bit gas
	utxoes, err := getLitecoinUTXO(fromAddress)
	if err != nil {
		return fmt.Errorf("fail to find utxo for address(%s): %w", fromAddr, err)
	}
	if len(utxoes) == 0 {
		return nil
	}
	size := estimateLTCTxSize(memo, utxoes)
	gas := size * gasRate
	redeemTx := wire.NewMsgTx(wire.TxVersion)
	totalAmt := float64(0)
	individualAmounts := make(map[string]ltcutil.Amount, len(utxoes))
	for _, item := range utxoes {
		txID, err := chainhash.NewHashFromStr(item.TxID)
		if err != nil {
			return fmt.Errorf("fail to create hash from string: %w", err)
		}
		// double check that the utxo is still valid
		outputPoint := wire.NewOutPoint(txID, item.Vout)
		sourceTxIn := wire.NewTxIn(outputPoint, nil, nil)
		redeemTx.AddTxIn(sourceTxIn)
		totalAmt += item.Amount
		amt, err := ltcutil.NewAmount(item.Amount)
		if err != nil {
			return fmt.Errorf("fail to parse amount(%f): %w", item.Amount, err)
		}
		individualAmounts[fmt.Sprintf("%s-%d", item.TxID, item.Vout)] = amt
	}
	buf, err := txscript.PayToAddrScript(toAddr)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	total, err := ltcutil.NewAmount(totalAmt)
	if err != nil {
		return fmt.Errorf("fail to parse total amount(%f),err: %w", totalAmt, err)
	}
	toCustomer := int64(total) - gas
	// pay to customer
	redeemTxOut := wire.NewTxOut(toCustomer, buf)
	redeemTx.AddTxOut(redeemTxOut)

	if len(memo) != 0 {
		// memo
		nullDataScript, err := txscript.NullDataScript([]byte(memo))
		if err != nil {
			return fmt.Errorf("fail to generate null data script: %w", err)
		}
		redeemTx.AddTxOut(wire.NewTxOut(0, nullDataScript))
	}
	// sort inputs and outputs
	// txsort.InPlaceSort(redeemTx)

	for idx, txIn := range redeemTx.TxIn {
		sigHashes := txscript.NewTxSigHashes(redeemTx)
		sig := txscript.NewPrivateKeySignable(pkey)
		outputAmount := int64(individualAmounts[fmt.Sprintf("%s-%d", txIn.PreviousOutPoint.Hash, txIn.PreviousOutPoint.Index)])
		witness, err := txscript.WitnessSignature(redeemTx, sigHashes, idx, outputAmount, payToSource, txscript.SigHashAll, sig, true)
		if err != nil {
			return fmt.Errorf("fail to get witness: %w", err)
		}

		redeemTx.TxIn[idx].Witness = witness
		flag := txscript.StandardVerifyFlags
		engine, err := txscript.NewEngine(payToSource, redeemTx, idx, flag, nil, nil, outputAmount)
		if err != nil {
			return fmt.Errorf("fail to create engine: %w", err)
		}
		if err := engine.Execute(); err != nil {
			return fmt.Errorf("fail to execute the script: %w", err)
		}
	}

	h, err := client.SendRawTransaction(redeemTx, true)
	if err != nil {
		return fmt.Errorf("fail to broadcast raw transaction: %w", err)
	}
	fmt.Println("LTC hash:", h.String())
	return nil
}
