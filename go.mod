module gitlab.com/thorchain/devops/manual-yggdrasil-return

go 1.16

replace (
	github.com/binance-chain/tss-lib => gitlab.com/thorchain/tss/tss-lib v0.0.0-20201118045712-70b2cb4bf916
	github.com/gogo/protobuf => github.com/regen-network/protobuf v1.3.2-alpha.regen.4
	github.com/zondax/ledger-go => github.com/binance-chain/ledger-go v0.9.1
)

require (
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/btcsuite/btcutil v1.0.2
	github.com/cosmos/cosmos-sdk v0.42.1
	github.com/ethereum/go-ethereum v1.9.25
	github.com/gcash/bchd v0.18.1
	github.com/gcash/bchutil v0.0.0-20210113190856-6ea28dff4000
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/ltcsuite/ltcd v0.20.1-beta.0.20201210074626-c807bfe31ef0
	github.com/ltcsuite/ltcutil v1.0.2-beta
	github.com/lunixbochs/vtclean v1.0.0 // indirect
	github.com/manifoldco/promptui v0.8.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/tendermint/tendermint v0.34.8
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/thorchain/bifrost/bchd-txscript v0.0.0-20210123055555-abb86a2e300a
	gitlab.com/thorchain/bifrost/ltcd-txscript v0.0.0-20210123055845-c0f9cad51f13
	gitlab.com/thorchain/binance-sdk v1.2.3-0.20210117202539-d569b6b9ba5d
	gitlab.com/thorchain/thornode v0.41.1-0.20210525051633-4439d93e4d98
	gitlab.com/thorchain/txscript v0.0.0-20200413023754-8aaf3443d92b
)
