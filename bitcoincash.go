package main

import (
	"encoding/hex"
	"errors"
	"fmt"

	"github.com/btcsuite/btcutil"
	"github.com/gcash/bchd/bchec"
	"github.com/gcash/bchd/btcjson"
	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchd/chaincfg/chainhash"
	"github.com/gcash/bchd/rpcclient"
	"github.com/gcash/bchd/wire"
	"github.com/gcash/bchutil"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"github.com/urfave/cli/v2"
	txscript "gitlab.com/thorchain/bifrost/bchd-txscript"
)

var (
	// BCH settings
	bchHost            string
	bchRpcUserName     string
	bchRpcUserPassword string
)

func getBitcoinCashClient() *rpcclient.Client {
	client, err := rpcclient.New(&rpcclient.ConnConfig{
		Host:         bchHost,
		User:         bchRpcUserName,
		Pass:         bchRpcUserPassword,
		DisableTLS:   true,
		HTTPPostMode: true,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}
func getBitcoinCashNetworkConfig(ctx *cli.Context) *chaincfg.Params {
	net := ctx.String("bch-network")
	switch net {
	case chaincfg.RegressionNetParams.Name:
		return &chaincfg.RegressionNetParams
	case chaincfg.MainNetParams.Name:
		return &chaincfg.MainNetParams
	case chaincfg.TestNet3Params.Name:
		return &chaincfg.TestNet3Params
	}
	return nil
}
func getBCHAddressBalance(ctx *cli.Context, address string) (float64, error) {
	client := getBitcoinCashClient()
	if len(address) == 0 {
		return 0.0, errors.New("please provide address")
	}
	addr, err := bchutil.DecodeAddress(address, getBitcoinCashNetworkConfig(ctx))
	if err != nil {
		panic(err)
	}
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []bchutil.Address{
		addr,
	})
	if err != nil {
		return 0.0, fmt.Errorf("fail to list account: %w", err)
	}
	if ctx.Bool("verbose") {
		fmt.Println("BCH address:", address, "utxos:", len(result))
	}
	var total float64
	for _, item := range result {
		total += item.Amount
	}
	return total, nil
}
func getBchAddressFromPrivateKey(ctx *cli.Context, priKey *bchec.PrivateKey) (string, error) {
	pubKeyBuf := priKey.PubKey().SerializeCompressed()
	secpPubKey := secp256k1.PubKey(pubKeyBuf)
	addrPubKeyHash, err := bchutil.NewAddressPubKeyHash(secpPubKey.Address().Bytes(), getBitcoinCashNetworkConfig(ctx))
	if err != nil {
		return "", fmt.Errorf("fail to get address pubkey hash address: %w", err)
	}
	return addrPubKeyHash.EncodeAddress(), nil
}
func estimateBCHTxSize(memo string, txes []btcjson.ListUnspentResult) int64 {
	// overhead - 10
	// Per input - 148
	// Per output - 34 , we might have 1 / 2 output , depends on the circumstances , here we only count 1  output , would rather underestimate
	// so we won't hit absurd hight fee issue
	// overhead for NULL DATA - 9 , len(memo) is the size of memo
	return int64(10 + 148*len(txes) + 34 + 9 + len([]byte(memo)))
}
func sendBitcoinCashAction(ctx *cli.Context, key, to string, gasRate int64, memo string) error {
	client := getBitcoinCashClient()
	fromPriKey, err := hex.DecodeString(key)
	if err != nil {
		return fmt.Errorf("fail to decode from private key: %w", err)
	}
	pkey, _ := bchec.PrivKeyFromBytes(bchec.S256(), fromPriKey)
	fromAddr, err := getBchAddressFromPrivateKey(ctx, pkey)
	if err != nil {
		return fmt.Errorf("fail to get address from private key: %w", err)
	}
	fromAddress, err := bchutil.DecodeAddress(fromAddr, getBitcoinCashNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode from address(%s): %w", fromAddr, err)
	}
	payToSource, err := txscript.PayToAddrScript(fromAddress)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	toAddr, err := bchutil.DecodeAddress(to, getBitcoinCashNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode address(%s): %w", to, err)
	}
	// add a little bit gas
	utxoes, err := getBitcoinCashUTXO(fromAddress)
	if err != nil {
		return fmt.Errorf("fail to find utxo for address(%s): %w", fromAddr, err)
	}
	if len(utxoes) == 0 {
		return nil
	}
	size := estimateBCHTxSize(memo, utxoes)
	gas := size * gasRate
	redeemTx := wire.NewMsgTx(wire.TxVersion)
	totalAmt := float64(0)
	individualAmounts := make(map[string]btcutil.Amount, len(utxoes))
	for _, item := range utxoes {
		txID, err := chainhash.NewHashFromStr(item.TxID)
		if err != nil {
			return fmt.Errorf("fail to create hash from string: %w", err)
		}
		// double check that the utxo is still valid
		outputPoint := wire.NewOutPoint(txID, item.Vout)
		sourceTxIn := wire.NewTxIn(outputPoint, nil)
		redeemTx.AddTxIn(sourceTxIn)
		totalAmt += item.Amount
		amt, err := btcutil.NewAmount(item.Amount)
		if err != nil {
			return fmt.Errorf("fail to parse amount(%f): %w", item.Amount, err)
		}
		individualAmounts[fmt.Sprintf("%s-%d", item.TxID, item.Vout)] = amt
	}
	buf, err := txscript.PayToAddrScript(toAddr)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	total, err := bchutil.NewAmount(totalAmt)
	if err != nil {
		return fmt.Errorf("fail to parse total amount(%f),err: %w", totalAmt, err)
	}
	toCustomer := int64(total) - gas
	// pay to customer
	redeemTxOut := wire.NewTxOut(toCustomer, buf)
	redeemTx.AddTxOut(redeemTxOut)

	if len(memo) != 0 {
		// memo
		nullDataScript, err := txscript.NullDataScript([]byte(memo))
		if err != nil {
			return fmt.Errorf("fail to generate null data script: %w", err)
		}
		redeemTx.AddTxOut(wire.NewTxOut(0, nullDataScript))
	}
	// sort inputs and outputs
	for idx, txIn := range redeemTx.TxIn {
		sig := txscript.NewPrivateKeySignable(pkey)
		key := fmt.Sprintf("%s-%d", txIn.PreviousOutPoint.Hash, txIn.PreviousOutPoint.Index)
		outputAmount := int64(individualAmounts[key])
		signature, err := txscript.RawTxInECDSASignature(redeemTx, idx, payToSource, txscript.SigHashAll, sig, outputAmount)
		if err != nil {
			return fmt.Errorf("fail to get witness: %w", err)
		}
		pkData := sig.GetPubKey().SerializeCompressed()
		sigscript, err := txscript.NewScriptBuilder().AddData(signature).AddData(pkData).Script()
		if err != nil {
			return fmt.Errorf("fail to build signature script: %w", err)
		}
		redeemTx.TxIn[idx].SignatureScript = sigscript
		flag := txscript.StandardVerifyFlags
		engine, err := txscript.NewEngine(payToSource, redeemTx, idx, flag, nil, nil, outputAmount)
		if err != nil {
			return fmt.Errorf("fail to create engine: %w", err)
		}
		if err := engine.Execute(); err != nil {
			return fmt.Errorf("fail to execute the script: %w", err)
		}
	}

	h, err := client.SendRawTransaction(redeemTx, true)
	if err != nil {
		return fmt.Errorf("fail to broadcast raw transaction: %w", err)
	}
	fmt.Println("BCH hash:", h.String())
	return nil
}

func getBitcoinCashUTXO(from bchutil.Address) ([]btcjson.ListUnspentResult, error) {
	client := getBitcoinCashClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []bchutil.Address{
		from,
	})
	if err != nil {
		return nil, fmt.Errorf("fail to get unspent utox: %w", err)
	}
	var utxo []btcjson.ListUnspentResult
	for _, item := range result {
		if item.Amount == 0 {
			continue
		}
		if item.Confirmations == 0 && item.Amount < 10000 {
			continue
		}
		utxo = append(utxo, item)
	}
	return utxo, nil
}
