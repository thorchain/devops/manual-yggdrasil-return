// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// RouterCoin is an auto generated low-level Go binding around an user-defined struct.
type RouterCoin struct {
	Asset  common.Address
	Amount *big.Int
}

// RouterABI is the input ABI used to generate the binding from.
const RouterABI = "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"rune\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"oldVault\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newVault\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"TransferAllowance\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"vault\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"TransferOut\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"oldVault\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newVault\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"indexed\":false,\"internalType\":\"structRouter.Coin[]\",\"name\":\"coins\",\"type\":\"tuple[]\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"VaultTransfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"RUNE\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"recipients\",\"type\":\"address[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structRouter.Coin[]\",\"name\":\"coins\",\"type\":\"tuple[]\"},{\"internalType\":\"string[]\",\"name\":\"memos\",\"type\":\"string[]\"}],\"name\":\"batchTransferOut\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"vault\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"router\",\"type\":\"address\"},{\"internalType\":\"addresspayable\",\"name\":\"asgard\",\"type\":\"address\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structRouter.Coin[]\",\"name\":\"coins\",\"type\":\"tuple[]\"},{\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"returnVaultAssets\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"router\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"newVault\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"transferAllowance\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"memo\",\"type\":\"string\"}],\"name\":\"transferOut\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"vaultAllowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

// RouterFuncSigs maps the 4-byte function signature to its string representation.
var RouterFuncSigs = map[string]string{
	"93e4eaa9": "RUNE()",
	"48f1651d": "batchTransferOut(address[],(address,uint256)[],string[])",
	"1fece7b4": "deposit(address,address,uint256,string)",
	"2923e82e": "returnVaultAssets(address,address,(address,uint256)[],string)",
	"1b738b32": "transferAllowance(address,address,address,uint256,string)",
	"574da717": "transferOut(address,address,uint256,string)",
	"03b6a673": "vaultAllowance(address,address)",
}

// RouterBin is the compiled bytecode used for deploying new contracts.
var RouterBin = "0x608060405234801561001057600080fd5b5060405161120e38038061120e83398101604081905261002f91610054565b600080546001600160a01b0319166001600160a01b0392909216919091179055610082565b600060208284031215610065578081fd5b81516001600160a01b038116811461007b578182fd5b9392505050565b61117d806100916000396000f3fe6080604052600436106100705760003560e01c80632923e82e1161004e5780632923e82e146100e057806348f1651d146100f3578063574da7171461010657806393e4eaa91461011957610070565b806303b6a673146100755780631b738b32146100ab5780631fece7b4146100cd575b600080fd5b34801561008157600080fd5b50610095610090366004610d0a565b61013b565b6040516100a291906110cb565b60405180910390f35b3480156100b757600080fd5b506100cb6100c6366004610dbc565b610158565b005b6100cb6100db366004610ca0565b6101dc565b6100cb6100ee366004610d42565b61040b565b6100cb610101366004610e39565b61056f565b6100cb610114366004610ca0565b6105e9565b34801561012557600080fd5b5061012e610773565b6040516100a29190610fa2565b600160209081526000928352604080842090915290825290205481565b6001600160a01b0385163014156101c857610174848484610782565b836001600160a01b0316336001600160a01b03167f05b90458f953d3fcb2d7fb25616a2fddeca749d0c47cc5c9832d0266b5346eea8585856040516101bb93929190611030565b60405180910390a36101d5565b6101d58585858585610829565b5050505050565b60006001600160a01b03841661025057349050846001600160a01b03168160405161020690610f9f565b60006040518083038185875af1925050503d8060008114610243576040519150601f19603f3d011682016040523d82523d6000602084013e610248565b606091505b5050506103b7565b6000546001600160a01b038581169116141561035357506000546040516302ccb1b360e41b815283916001600160a01b031690632ccb1b30906102999030908590600401610fda565b602060405180830381600087803b1580156102b357600080fd5b505af11580156102c7573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906102eb9190610f1c565b50600054604051630852cd8d60e31b81526001600160a01b03909116906342966c689061031c9086906004016110cb565b600060405180830381600087803b15801561033657600080fd5b505af115801561034a573d6000803e3d6000fd5b505050506103b7565b61035d848461095f565b6001600160a01b038087166000908152600160209081526040808320938916835292905220549091506103909082610af2565b6001600160a01b038087166000908152600160209081526040808320938916835292905220555b836001600160a01b0316856001600160a01b03167fef519b7eb82aaf6ac376a6df2d793843ebfd593de5f1a0601d3cc6ab49ebb39583856040516103fc9291906110d4565b60405180910390a35050505050565b6001600160a01b0384163014156104bc5760005b8251811015610469576104618484838151811061043857fe5b60200260200101516000015185848151811061045057fe5b602002602001015160200151610782565b60010161041f565b50826001600160a01b0316336001600160a01b03167f281daef48d91e5cd3d32db0784f6af69cd8d8d2e8c612a3568dca51ded51e08f84846040516104af929190611060565b60405180910390a361050d565b60005b825181101561050b5761050385858584815181106104d957fe5b6020026020010151600001518685815181106104f157fe5b60200260200101516020015186610829565b6001016104bf565b505b826001600160a01b03163460405161052490610f9f565b60006040518083038185875af1925050503d8060008114610561576040519150601f19603f3d011682016040523d82523d6000602084013e610566565b606091505b50505050505050565b60005b82518110156105e3576105db84828151811061058a57fe5b602002602001015184838151811061059e57fe5b6020026020010151600001518584815181106105b657fe5b6020026020010151602001518585815181106105ce57fe5b60200260200101516105e9565b600101610572565b50505050565b60006001600160a01b03841661065d57349050846001600160a01b03163460405161061390610f9f565b60006040518083038185875af1925050503d8060008114610650576040519150601f19603f3d011682016040523d82523d6000602084013e610655565b606091505b50505061072c565b3360009081526001602090815260408083206001600160a01b038816845290915290205461068b9084610b17565b3360009081526001602090815260408083206001600160a01b038916808552925291829020929092555163a9059cbb60e01b815263a9059cbb906106d59088908790600401610fda565b602060405180830381600087803b1580156106ef57600080fd5b505af1158015610703573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906107279190610f1c565b508290505b846001600160a01b0316336001600160a01b03167fa9cd03aa3c1b4515114539cd53d22085129d495cb9e9f9af77864526240f1bf78684866040516103fc93929190611030565b6000546001600160a01b031681565b3360009081526001602090815260408083206001600160a01b03861684529091529020546107b09082610b17565b3360009081526001602081815260408084206001600160a01b038881168087529184528286209690965594881684529181528183209383529290925220546107f89082610af2565b6001600160a01b03938416600090815260016020908152604080832095909616825293909352929091209190915550565b3360009081526001602090815260408083206001600160a01b03871684529091529020546108579083610b17565b3360009081526001602090815260408083206001600160a01b038816808552925291829020929092555163095ea7b360e01b815263095ea7b3906108a19088908690600401610fda565b602060405180830381600087803b1580156108bb57600080fd5b505af11580156108cf573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906108f39190610f1c565b506040516307fb39ed60e21b81526001600160a01b03861690631fece7b490610926908790879087908790600401610ff3565b600060405180830381600087803b15801561094057600080fd5b505af1158015610954573d6000803e3d6000fd5b505050505050505050565b600080836001600160a01b03166370a08231306040518263ffffffff1660e01b815260040161098e9190610fa2565b60206040518083038186803b1580156109a657600080fd5b505afa1580156109ba573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906109de9190610f3c565b6040516323b872dd60e01b81529091506001600160a01b038516906323b872dd90610a1190339030908890600401610fb6565b602060405180830381600087803b158015610a2b57600080fd5b505af1158015610a3f573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610a639190610f1c565b50610aea81856001600160a01b03166370a08231306040518263ffffffff1660e01b8152600401610a949190610fa2565b60206040518083038186803b158015610aac57600080fd5b505afa158015610ac0573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610ae49190610f3c565b90610b17565b949350505050565b6000828201838110801590610b075750828110155b610b1057600080fd5b9392505050565b600082821115610b2657600080fd5b50900390565b600082601f830112610b3c578081fd5b81356020610b51610b4c83611111565b6110ed565b82815281810190858301855b85811015610b8657610b74898684358b0101610c3b565b84529284019290840190600101610b5d565b5090979650505050505050565b600082601f830112610ba3578081fd5b81356020610bb3610b4c83611111565b82815281810190858301604080860288018501891015610bd1578687fd5b865b86811015610c2d5781838b031215610be9578788fd5b815182810181811067ffffffffffffffff82111715610c0457fe5b83528335610c118161112f565b8152838701358782015285529385019391810191600101610bd3565b509198975050505050505050565b600082601f830112610c4b578081fd5b813567ffffffffffffffff811115610c5f57fe5b610c72601f8201601f19166020016110ed565b818152846020838601011115610c86578283fd5b816020850160208301379081016020019190915292915050565b60008060008060808587031215610cb5578384fd5b8435610cc08161112f565b93506020850135610cd08161112f565b925060408501359150606085013567ffffffffffffffff811115610cf2578182fd5b610cfe87828801610c3b565b91505092959194509250565b60008060408385031215610d1c578182fd5b8235610d278161112f565b91506020830135610d378161112f565b809150509250929050565b60008060008060808587031215610d57578384fd5b8435610d628161112f565b93506020850135610d728161112f565b9250604085013567ffffffffffffffff80821115610d8e578384fd5b610d9a88838901610b93565b93506060870135915080821115610daf578283fd5b50610cfe87828801610c3b565b600080600080600060a08688031215610dd3578081fd5b8535610dde8161112f565b94506020860135610dee8161112f565b93506040860135610dfe8161112f565b925060608601359150608086013567ffffffffffffffff811115610e20578182fd5b610e2c88828901610c3b565b9150509295509295909350565b600080600060608486031215610e4d578283fd5b833567ffffffffffffffff80821115610e64578485fd5b818601915086601f830112610e77578485fd5b81356020610e87610b4c83611111565b82815281810190858301838502870184018c1015610ea357898afd5b8996505b84871015610ece578035610eba8161112f565b835260019690960195918301918301610ea7565b5097505087013592505080821115610ee4578384fd5b610ef087838801610b93565b93506040860135915080821115610f05578283fd5b50610f1286828701610b2c565b9150509250925092565b600060208284031215610f2d578081fd5b81518015158114610b10578182fd5b600060208284031215610f4d578081fd5b5051919050565b60008151808452815b81811015610f7957602081850181015186830182015201610f5d565b81811115610f8a5782602083870101525b50601f01601f19169290920160200192915050565b90565b6001600160a01b0391909116815260200190565b6001600160a01b039384168152919092166020820152604081019190915260600190565b6001600160a01b03929092168252602082015260400190565b6001600160a01b038581168252841660208201526040810183905260806060820181905260009061102690830184610f54565b9695505050505050565b600060018060a01b0385168252836020830152606060408301526110576060830184610f54565b95945050505050565b60408082528351828201819052600091906020906060850190828801855b828110156110ac57815180516001600160a01b0316855285015185850152928501929084019060010161107e565b505050848103828601526110c08187610f54565b979650505050505050565b90815260200190565b600083825260406020830152610aea6040830184610f54565b60405181810167ffffffffffffffff8111828210171561110957fe5b604052919050565b600067ffffffffffffffff82111561112557fe5b5060209081020190565b6001600160a01b038116811461114457600080fd5b5056fea264697066735822122017a84638fa7b1c33cf07d5b7f5d6660f0a9a6e548b53c2b9e970b86df99e8aa464736f6c63430007060033"

// DeployRouter deploys a new Ethereum contract, binding an instance of Router to it.
func DeployRouter(auth *bind.TransactOpts, backend bind.ContractBackend, rune common.Address) (common.Address, *types.Transaction, *Router, error) {
	parsed, err := abi.JSON(strings.NewReader(RouterABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(RouterBin), backend, rune)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Router{RouterCaller: RouterCaller{contract: contract}, RouterTransactor: RouterTransactor{contract: contract}, RouterFilterer: RouterFilterer{contract: contract}}, nil
}

// Router is an auto generated Go binding around an Ethereum contract.
type Router struct {
	RouterCaller     // Read-only binding to the contract
	RouterTransactor // Write-only binding to the contract
	RouterFilterer   // Log filterer for contract events
}

// RouterCaller is an auto generated read-only Go binding around an Ethereum contract.
type RouterCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RouterTransactor is an auto generated write-only Go binding around an Ethereum contract.
type RouterTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RouterFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type RouterFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RouterSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type RouterSession struct {
	Contract     *Router           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// RouterCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type RouterCallerSession struct {
	Contract *RouterCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// RouterTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type RouterTransactorSession struct {
	Contract     *RouterTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// RouterRaw is an auto generated low-level Go binding around an Ethereum contract.
type RouterRaw struct {
	Contract *Router // Generic contract binding to access the raw methods on
}

// RouterCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type RouterCallerRaw struct {
	Contract *RouterCaller // Generic read-only contract binding to access the raw methods on
}

// RouterTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type RouterTransactorRaw struct {
	Contract *RouterTransactor // Generic write-only contract binding to access the raw methods on
}

// NewRouter creates a new instance of Router, bound to a specific deployed contract.
func NewRouter(address common.Address, backend bind.ContractBackend) (*Router, error) {
	contract, err := bindRouter(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Router{RouterCaller: RouterCaller{contract: contract}, RouterTransactor: RouterTransactor{contract: contract}, RouterFilterer: RouterFilterer{contract: contract}}, nil
}

// NewRouterCaller creates a new read-only instance of Router, bound to a specific deployed contract.
func NewRouterCaller(address common.Address, caller bind.ContractCaller) (*RouterCaller, error) {
	contract, err := bindRouter(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &RouterCaller{contract: contract}, nil
}

// NewRouterTransactor creates a new write-only instance of Router, bound to a specific deployed contract.
func NewRouterTransactor(address common.Address, transactor bind.ContractTransactor) (*RouterTransactor, error) {
	contract, err := bindRouter(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &RouterTransactor{contract: contract}, nil
}

// NewRouterFilterer creates a new log filterer instance of Router, bound to a specific deployed contract.
func NewRouterFilterer(address common.Address, filterer bind.ContractFilterer) (*RouterFilterer, error) {
	contract, err := bindRouter(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &RouterFilterer{contract: contract}, nil
}

// bindRouter binds a generic wrapper to an already deployed contract.
func bindRouter(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(RouterABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Router *RouterRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Router.Contract.RouterCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Router *RouterRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Router.Contract.RouterTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Router *RouterRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Router.Contract.RouterTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Router *RouterCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Router.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Router *RouterTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Router.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Router *RouterTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Router.Contract.contract.Transact(opts, method, params...)
}

// RUNE is a free data retrieval call binding the contract method 0x93e4eaa9.
//
// Solidity: function RUNE() view returns(address)
func (_Router *RouterCaller) RUNE(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Router.contract.Call(opts, &out, "RUNE")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// RUNE is a free data retrieval call binding the contract method 0x93e4eaa9.
//
// Solidity: function RUNE() view returns(address)
func (_Router *RouterSession) RUNE() (common.Address, error) {
	return _Router.Contract.RUNE(&_Router.CallOpts)
}

// RUNE is a free data retrieval call binding the contract method 0x93e4eaa9.
//
// Solidity: function RUNE() view returns(address)
func (_Router *RouterCallerSession) RUNE() (common.Address, error) {
	return _Router.Contract.RUNE(&_Router.CallOpts)
}

// VaultAllowance is a free data retrieval call binding the contract method 0x03b6a673.
//
// Solidity: function vaultAllowance(address , address ) view returns(uint256)
func (_Router *RouterCaller) VaultAllowance(opts *bind.CallOpts, arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Router.contract.Call(opts, &out, "vaultAllowance", arg0, arg1)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// VaultAllowance is a free data retrieval call binding the contract method 0x03b6a673.
//
// Solidity: function vaultAllowance(address , address ) view returns(uint256)
func (_Router *RouterSession) VaultAllowance(arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	return _Router.Contract.VaultAllowance(&_Router.CallOpts, arg0, arg1)
}

// VaultAllowance is a free data retrieval call binding the contract method 0x03b6a673.
//
// Solidity: function vaultAllowance(address , address ) view returns(uint256)
func (_Router *RouterCallerSession) VaultAllowance(arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	return _Router.Contract.VaultAllowance(&_Router.CallOpts, arg0, arg1)
}

// BatchTransferOut is a paid mutator transaction binding the contract method 0x48f1651d.
//
// Solidity: function batchTransferOut(address[] recipients, (address,uint256)[] coins, string[] memos) payable returns()
func (_Router *RouterTransactor) BatchTransferOut(opts *bind.TransactOpts, recipients []common.Address, coins []RouterCoin, memos []string) (*types.Transaction, error) {
	return _Router.contract.Transact(opts, "batchTransferOut", recipients, coins, memos)
}

// BatchTransferOut is a paid mutator transaction binding the contract method 0x48f1651d.
//
// Solidity: function batchTransferOut(address[] recipients, (address,uint256)[] coins, string[] memos) payable returns()
func (_Router *RouterSession) BatchTransferOut(recipients []common.Address, coins []RouterCoin, memos []string) (*types.Transaction, error) {
	return _Router.Contract.BatchTransferOut(&_Router.TransactOpts, recipients, coins, memos)
}

// BatchTransferOut is a paid mutator transaction binding the contract method 0x48f1651d.
//
// Solidity: function batchTransferOut(address[] recipients, (address,uint256)[] coins, string[] memos) payable returns()
func (_Router *RouterTransactorSession) BatchTransferOut(recipients []common.Address, coins []RouterCoin, memos []string) (*types.Transaction, error) {
	return _Router.Contract.BatchTransferOut(&_Router.TransactOpts, recipients, coins, memos)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address vault, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterTransactor) Deposit(opts *bind.TransactOpts, vault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.contract.Transact(opts, "deposit", vault, asset, amount, memo)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address vault, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterSession) Deposit(vault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.Deposit(&_Router.TransactOpts, vault, asset, amount, memo)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address vault, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterTransactorSession) Deposit(vault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.Deposit(&_Router.TransactOpts, vault, asset, amount, memo)
}

// ReturnVaultAssets is a paid mutator transaction binding the contract method 0x2923e82e.
//
// Solidity: function returnVaultAssets(address router, address asgard, (address,uint256)[] coins, string memo) payable returns()
func (_Router *RouterTransactor) ReturnVaultAssets(opts *bind.TransactOpts, router common.Address, asgard common.Address, coins []RouterCoin, memo string) (*types.Transaction, error) {
	return _Router.contract.Transact(opts, "returnVaultAssets", router, asgard, coins, memo)
}

// ReturnVaultAssets is a paid mutator transaction binding the contract method 0x2923e82e.
//
// Solidity: function returnVaultAssets(address router, address asgard, (address,uint256)[] coins, string memo) payable returns()
func (_Router *RouterSession) ReturnVaultAssets(router common.Address, asgard common.Address, coins []RouterCoin, memo string) (*types.Transaction, error) {
	return _Router.Contract.ReturnVaultAssets(&_Router.TransactOpts, router, asgard, coins, memo)
}

// ReturnVaultAssets is a paid mutator transaction binding the contract method 0x2923e82e.
//
// Solidity: function returnVaultAssets(address router, address asgard, (address,uint256)[] coins, string memo) payable returns()
func (_Router *RouterTransactorSession) ReturnVaultAssets(router common.Address, asgard common.Address, coins []RouterCoin, memo string) (*types.Transaction, error) {
	return _Router.Contract.ReturnVaultAssets(&_Router.TransactOpts, router, asgard, coins, memo)
}

// TransferAllowance is a paid mutator transaction binding the contract method 0x1b738b32.
//
// Solidity: function transferAllowance(address router, address newVault, address asset, uint256 amount, string memo) returns()
func (_Router *RouterTransactor) TransferAllowance(opts *bind.TransactOpts, router common.Address, newVault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.contract.Transact(opts, "transferAllowance", router, newVault, asset, amount, memo)
}

// TransferAllowance is a paid mutator transaction binding the contract method 0x1b738b32.
//
// Solidity: function transferAllowance(address router, address newVault, address asset, uint256 amount, string memo) returns()
func (_Router *RouterSession) TransferAllowance(router common.Address, newVault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.TransferAllowance(&_Router.TransactOpts, router, newVault, asset, amount, memo)
}

// TransferAllowance is a paid mutator transaction binding the contract method 0x1b738b32.
//
// Solidity: function transferAllowance(address router, address newVault, address asset, uint256 amount, string memo) returns()
func (_Router *RouterTransactorSession) TransferAllowance(router common.Address, newVault common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.TransferAllowance(&_Router.TransactOpts, router, newVault, asset, amount, memo)
}

// TransferOut is a paid mutator transaction binding the contract method 0x574da717.
//
// Solidity: function transferOut(address to, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterTransactor) TransferOut(opts *bind.TransactOpts, to common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.contract.Transact(opts, "transferOut", to, asset, amount, memo)
}

// TransferOut is a paid mutator transaction binding the contract method 0x574da717.
//
// Solidity: function transferOut(address to, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterSession) TransferOut(to common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.TransferOut(&_Router.TransactOpts, to, asset, amount, memo)
}

// TransferOut is a paid mutator transaction binding the contract method 0x574da717.
//
// Solidity: function transferOut(address to, address asset, uint256 amount, string memo) payable returns()
func (_Router *RouterTransactorSession) TransferOut(to common.Address, asset common.Address, amount *big.Int, memo string) (*types.Transaction, error) {
	return _Router.Contract.TransferOut(&_Router.TransactOpts, to, asset, amount, memo)
}

// RouterDepositIterator is returned from FilterDeposit and is used to iterate over the raw logs and unpacked data for Deposit events raised by the Router contract.
type RouterDepositIterator struct {
	Event *RouterDeposit // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RouterDepositIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RouterDeposit)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RouterDeposit)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RouterDepositIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RouterDepositIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RouterDeposit represents a Deposit event raised by the Router contract.
type RouterDeposit struct {
	To     common.Address
	Asset  common.Address
	Amount *big.Int
	Memo   string
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterDeposit is a free log retrieval operation binding the contract event 0xef519b7eb82aaf6ac376a6df2d793843ebfd593de5f1a0601d3cc6ab49ebb395.
//
// Solidity: event Deposit(address indexed to, address indexed asset, uint256 amount, string memo)
func (_Router *RouterFilterer) FilterDeposit(opts *bind.FilterOpts, to []common.Address, asset []common.Address) (*RouterDepositIterator, error) {

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var assetRule []interface{}
	for _, assetItem := range asset {
		assetRule = append(assetRule, assetItem)
	}

	logs, sub, err := _Router.contract.FilterLogs(opts, "Deposit", toRule, assetRule)
	if err != nil {
		return nil, err
	}
	return &RouterDepositIterator{contract: _Router.contract, event: "Deposit", logs: logs, sub: sub}, nil
}

// WatchDeposit is a free log subscription operation binding the contract event 0xef519b7eb82aaf6ac376a6df2d793843ebfd593de5f1a0601d3cc6ab49ebb395.
//
// Solidity: event Deposit(address indexed to, address indexed asset, uint256 amount, string memo)
func (_Router *RouterFilterer) WatchDeposit(opts *bind.WatchOpts, sink chan<- *RouterDeposit, to []common.Address, asset []common.Address) (event.Subscription, error) {

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var assetRule []interface{}
	for _, assetItem := range asset {
		assetRule = append(assetRule, assetItem)
	}

	logs, sub, err := _Router.contract.WatchLogs(opts, "Deposit", toRule, assetRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RouterDeposit)
				if err := _Router.contract.UnpackLog(event, "Deposit", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDeposit is a log parse operation binding the contract event 0xef519b7eb82aaf6ac376a6df2d793843ebfd593de5f1a0601d3cc6ab49ebb395.
//
// Solidity: event Deposit(address indexed to, address indexed asset, uint256 amount, string memo)
func (_Router *RouterFilterer) ParseDeposit(log types.Log) (*RouterDeposit, error) {
	event := new(RouterDeposit)
	if err := _Router.contract.UnpackLog(event, "Deposit", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// RouterTransferAllowanceIterator is returned from FilterTransferAllowance and is used to iterate over the raw logs and unpacked data for TransferAllowance events raised by the Router contract.
type RouterTransferAllowanceIterator struct {
	Event *RouterTransferAllowance // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RouterTransferAllowanceIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RouterTransferAllowance)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RouterTransferAllowance)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RouterTransferAllowanceIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RouterTransferAllowanceIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RouterTransferAllowance represents a TransferAllowance event raised by the Router contract.
type RouterTransferAllowance struct {
	OldVault common.Address
	NewVault common.Address
	Asset    common.Address
	Amount   *big.Int
	Memo     string
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterTransferAllowance is a free log retrieval operation binding the contract event 0x05b90458f953d3fcb2d7fb25616a2fddeca749d0c47cc5c9832d0266b5346eea.
//
// Solidity: event TransferAllowance(address indexed oldVault, address indexed newVault, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) FilterTransferAllowance(opts *bind.FilterOpts, oldVault []common.Address, newVault []common.Address) (*RouterTransferAllowanceIterator, error) {

	var oldVaultRule []interface{}
	for _, oldVaultItem := range oldVault {
		oldVaultRule = append(oldVaultRule, oldVaultItem)
	}
	var newVaultRule []interface{}
	for _, newVaultItem := range newVault {
		newVaultRule = append(newVaultRule, newVaultItem)
	}

	logs, sub, err := _Router.contract.FilterLogs(opts, "TransferAllowance", oldVaultRule, newVaultRule)
	if err != nil {
		return nil, err
	}
	return &RouterTransferAllowanceIterator{contract: _Router.contract, event: "TransferAllowance", logs: logs, sub: sub}, nil
}

// WatchTransferAllowance is a free log subscription operation binding the contract event 0x05b90458f953d3fcb2d7fb25616a2fddeca749d0c47cc5c9832d0266b5346eea.
//
// Solidity: event TransferAllowance(address indexed oldVault, address indexed newVault, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) WatchTransferAllowance(opts *bind.WatchOpts, sink chan<- *RouterTransferAllowance, oldVault []common.Address, newVault []common.Address) (event.Subscription, error) {

	var oldVaultRule []interface{}
	for _, oldVaultItem := range oldVault {
		oldVaultRule = append(oldVaultRule, oldVaultItem)
	}
	var newVaultRule []interface{}
	for _, newVaultItem := range newVault {
		newVaultRule = append(newVaultRule, newVaultItem)
	}

	logs, sub, err := _Router.contract.WatchLogs(opts, "TransferAllowance", oldVaultRule, newVaultRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RouterTransferAllowance)
				if err := _Router.contract.UnpackLog(event, "TransferAllowance", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransferAllowance is a log parse operation binding the contract event 0x05b90458f953d3fcb2d7fb25616a2fddeca749d0c47cc5c9832d0266b5346eea.
//
// Solidity: event TransferAllowance(address indexed oldVault, address indexed newVault, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) ParseTransferAllowance(log types.Log) (*RouterTransferAllowance, error) {
	event := new(RouterTransferAllowance)
	if err := _Router.contract.UnpackLog(event, "TransferAllowance", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// RouterTransferOutIterator is returned from FilterTransferOut and is used to iterate over the raw logs and unpacked data for TransferOut events raised by the Router contract.
type RouterTransferOutIterator struct {
	Event *RouterTransferOut // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RouterTransferOutIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RouterTransferOut)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RouterTransferOut)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RouterTransferOutIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RouterTransferOutIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RouterTransferOut represents a TransferOut event raised by the Router contract.
type RouterTransferOut struct {
	Vault  common.Address
	To     common.Address
	Asset  common.Address
	Amount *big.Int
	Memo   string
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterTransferOut is a free log retrieval operation binding the contract event 0xa9cd03aa3c1b4515114539cd53d22085129d495cb9e9f9af77864526240f1bf7.
//
// Solidity: event TransferOut(address indexed vault, address indexed to, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) FilterTransferOut(opts *bind.FilterOpts, vault []common.Address, to []common.Address) (*RouterTransferOutIterator, error) {

	var vaultRule []interface{}
	for _, vaultItem := range vault {
		vaultRule = append(vaultRule, vaultItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Router.contract.FilterLogs(opts, "TransferOut", vaultRule, toRule)
	if err != nil {
		return nil, err
	}
	return &RouterTransferOutIterator{contract: _Router.contract, event: "TransferOut", logs: logs, sub: sub}, nil
}

// WatchTransferOut is a free log subscription operation binding the contract event 0xa9cd03aa3c1b4515114539cd53d22085129d495cb9e9f9af77864526240f1bf7.
//
// Solidity: event TransferOut(address indexed vault, address indexed to, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) WatchTransferOut(opts *bind.WatchOpts, sink chan<- *RouterTransferOut, vault []common.Address, to []common.Address) (event.Subscription, error) {

	var vaultRule []interface{}
	for _, vaultItem := range vault {
		vaultRule = append(vaultRule, vaultItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _Router.contract.WatchLogs(opts, "TransferOut", vaultRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RouterTransferOut)
				if err := _Router.contract.UnpackLog(event, "TransferOut", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransferOut is a log parse operation binding the contract event 0xa9cd03aa3c1b4515114539cd53d22085129d495cb9e9f9af77864526240f1bf7.
//
// Solidity: event TransferOut(address indexed vault, address indexed to, address asset, uint256 amount, string memo)
func (_Router *RouterFilterer) ParseTransferOut(log types.Log) (*RouterTransferOut, error) {
	event := new(RouterTransferOut)
	if err := _Router.contract.UnpackLog(event, "TransferOut", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// RouterVaultTransferIterator is returned from FilterVaultTransfer and is used to iterate over the raw logs and unpacked data for VaultTransfer events raised by the Router contract.
type RouterVaultTransferIterator struct {
	Event *RouterVaultTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RouterVaultTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RouterVaultTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RouterVaultTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RouterVaultTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RouterVaultTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RouterVaultTransfer represents a VaultTransfer event raised by the Router contract.
type RouterVaultTransfer struct {
	OldVault common.Address
	NewVault common.Address
	Coins    []RouterCoin
	Memo     string
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterVaultTransfer is a free log retrieval operation binding the contract event 0x281daef48d91e5cd3d32db0784f6af69cd8d8d2e8c612a3568dca51ded51e08f.
//
// Solidity: event VaultTransfer(address indexed oldVault, address indexed newVault, (address,uint256)[] coins, string memo)
func (_Router *RouterFilterer) FilterVaultTransfer(opts *bind.FilterOpts, oldVault []common.Address, newVault []common.Address) (*RouterVaultTransferIterator, error) {

	var oldVaultRule []interface{}
	for _, oldVaultItem := range oldVault {
		oldVaultRule = append(oldVaultRule, oldVaultItem)
	}
	var newVaultRule []interface{}
	for _, newVaultItem := range newVault {
		newVaultRule = append(newVaultRule, newVaultItem)
	}

	logs, sub, err := _Router.contract.FilterLogs(opts, "VaultTransfer", oldVaultRule, newVaultRule)
	if err != nil {
		return nil, err
	}
	return &RouterVaultTransferIterator{contract: _Router.contract, event: "VaultTransfer", logs: logs, sub: sub}, nil
}

// WatchVaultTransfer is a free log subscription operation binding the contract event 0x281daef48d91e5cd3d32db0784f6af69cd8d8d2e8c612a3568dca51ded51e08f.
//
// Solidity: event VaultTransfer(address indexed oldVault, address indexed newVault, (address,uint256)[] coins, string memo)
func (_Router *RouterFilterer) WatchVaultTransfer(opts *bind.WatchOpts, sink chan<- *RouterVaultTransfer, oldVault []common.Address, newVault []common.Address) (event.Subscription, error) {

	var oldVaultRule []interface{}
	for _, oldVaultItem := range oldVault {
		oldVaultRule = append(oldVaultRule, oldVaultItem)
	}
	var newVaultRule []interface{}
	for _, newVaultItem := range newVault {
		newVaultRule = append(newVaultRule, newVaultItem)
	}

	logs, sub, err := _Router.contract.WatchLogs(opts, "VaultTransfer", oldVaultRule, newVaultRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RouterVaultTransfer)
				if err := _Router.contract.UnpackLog(event, "VaultTransfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseVaultTransfer is a log parse operation binding the contract event 0x281daef48d91e5cd3d32db0784f6af69cd8d8d2e8c612a3568dca51ded51e08f.
//
// Solidity: event VaultTransfer(address indexed oldVault, address indexed newVault, (address,uint256)[] coins, string memo)
func (_Router *RouterFilterer) ParseVaultTransfer(log types.Log) (*RouterVaultTransfer, error) {
	event := new(RouterVaultTransfer)
	if err := _Router.contract.UnpackLog(event, "VaultTransfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// SafeMathABI is the input ABI used to generate the binding from.
const SafeMathABI = "[]"

// SafeMathBin is the compiled bytecode used for deploying new contracts.
var SafeMathBin = "0x60566023600b82828239805160001a607314601657fe5b30600052607381538281f3fe73000000000000000000000000000000000000000030146080604052600080fdfea2646970667358221220546581ae3b0d523950df78b394081ff569690896c043341161ea420f4987a5b764736f6c63430007060033"

// DeploySafeMath deploys a new Ethereum contract, binding an instance of SafeMath to it.
func DeploySafeMath(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SafeMath, error) {
	parsed, err := abi.JSON(strings.NewReader(SafeMathABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(SafeMathBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SafeMath{SafeMathCaller: SafeMathCaller{contract: contract}, SafeMathTransactor: SafeMathTransactor{contract: contract}, SafeMathFilterer: SafeMathFilterer{contract: contract}}, nil
}

// SafeMath is an auto generated Go binding around an Ethereum contract.
type SafeMath struct {
	SafeMathCaller     // Read-only binding to the contract
	SafeMathTransactor // Write-only binding to the contract
	SafeMathFilterer   // Log filterer for contract events
}

// SafeMathCaller is an auto generated read-only Go binding around an Ethereum contract.
type SafeMathCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathTransactor is an auto generated write-only Go binding around an Ethereum contract.
type SafeMathTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SafeMathFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SafeMathSession struct {
	Contract     *SafeMath         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SafeMathCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SafeMathCallerSession struct {
	Contract *SafeMathCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// SafeMathTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SafeMathTransactorSession struct {
	Contract     *SafeMathTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// SafeMathRaw is an auto generated low-level Go binding around an Ethereum contract.
type SafeMathRaw struct {
	Contract *SafeMath // Generic contract binding to access the raw methods on
}

// SafeMathCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SafeMathCallerRaw struct {
	Contract *SafeMathCaller // Generic read-only contract binding to access the raw methods on
}

// SafeMathTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SafeMathTransactorRaw struct {
	Contract *SafeMathTransactor // Generic write-only contract binding to access the raw methods on
}

// NewSafeMath creates a new instance of SafeMath, bound to a specific deployed contract.
func NewSafeMath(address common.Address, backend bind.ContractBackend) (*SafeMath, error) {
	contract, err := bindSafeMath(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SafeMath{SafeMathCaller: SafeMathCaller{contract: contract}, SafeMathTransactor: SafeMathTransactor{contract: contract}, SafeMathFilterer: SafeMathFilterer{contract: contract}}, nil
}

// NewSafeMathCaller creates a new read-only instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathCaller(address common.Address, caller bind.ContractCaller) (*SafeMathCaller, error) {
	contract, err := bindSafeMath(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SafeMathCaller{contract: contract}, nil
}

// NewSafeMathTransactor creates a new write-only instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathTransactor(address common.Address, transactor bind.ContractTransactor) (*SafeMathTransactor, error) {
	contract, err := bindSafeMath(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SafeMathTransactor{contract: contract}, nil
}

// NewSafeMathFilterer creates a new log filterer instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathFilterer(address common.Address, filterer bind.ContractFilterer) (*SafeMathFilterer, error) {
	contract, err := bindSafeMath(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SafeMathFilterer{contract: contract}, nil
}

// bindSafeMath binds a generic wrapper to an already deployed contract.
func bindSafeMath(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SafeMathABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeMath *SafeMathRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeMath.Contract.SafeMathCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeMath *SafeMathRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeMath.Contract.SafeMathTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeMath *SafeMathRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeMath.Contract.SafeMathTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeMath *SafeMathCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeMath.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeMath *SafeMathTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeMath.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeMath *SafeMathTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeMath.Contract.contract.Transact(opts, method, params...)
}

// IERC20ABI is the input ABI used to generate the binding from.
const IERC20ABI = "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"burn\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// IERC20FuncSigs maps the 4-byte function signature to its string representation.
var IERC20FuncSigs = map[string]string{
	"095ea7b3": "approve(address,uint256)",
	"70a08231": "balanceOf(address)",
	"42966c68": "burn(uint256)",
	"a9059cbb": "transfer(address,uint256)",
	"23b872dd": "transferFrom(address,address,uint256)",
}

// IERC20 is an auto generated Go binding around an Ethereum contract.
type IERC20 struct {
	IERC20Caller     // Read-only binding to the contract
	IERC20Transactor // Write-only binding to the contract
	IERC20Filterer   // Log filterer for contract events
}

// IERC20Caller is an auto generated read-only Go binding around an Ethereum contract.
type IERC20Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Transactor is an auto generated write-only Go binding around an Ethereum contract.
type IERC20Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IERC20Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IERC20Session struct {
	Contract     *IERC20           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IERC20CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IERC20CallerSession struct {
	Contract *IERC20Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IERC20TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IERC20TransactorSession struct {
	Contract     *IERC20Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IERC20Raw is an auto generated low-level Go binding around an Ethereum contract.
type IERC20Raw struct {
	Contract *IERC20 // Generic contract binding to access the raw methods on
}

// IERC20CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IERC20CallerRaw struct {
	Contract *IERC20Caller // Generic read-only contract binding to access the raw methods on
}

// IERC20TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IERC20TransactorRaw struct {
	Contract *IERC20Transactor // Generic write-only contract binding to access the raw methods on
}

// NewIERC20 creates a new instance of IERC20, bound to a specific deployed contract.
func NewIERC20(address common.Address, backend bind.ContractBackend) (*IERC20, error) {
	contract, err := bindIERC20(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IERC20{IERC20Caller: IERC20Caller{contract: contract}, IERC20Transactor: IERC20Transactor{contract: contract}, IERC20Filterer: IERC20Filterer{contract: contract}}, nil
}

// NewIERC20Caller creates a new read-only instance of IERC20, bound to a specific deployed contract.
func NewIERC20Caller(address common.Address, caller bind.ContractCaller) (*IERC20Caller, error) {
	contract, err := bindIERC20(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IERC20Caller{contract: contract}, nil
}

// NewIERC20Transactor creates a new write-only instance of IERC20, bound to a specific deployed contract.
func NewIERC20Transactor(address common.Address, transactor bind.ContractTransactor) (*IERC20Transactor, error) {
	contract, err := bindIERC20(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IERC20Transactor{contract: contract}, nil
}

// NewIERC20Filterer creates a new log filterer instance of IERC20, bound to a specific deployed contract.
func NewIERC20Filterer(address common.Address, filterer bind.ContractFilterer) (*IERC20Filterer, error) {
	contract, err := bindIERC20(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IERC20Filterer{contract: contract}, nil
}

// bindIERC20 binds a generic wrapper to an already deployed contract.
func bindIERC20(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IERC20ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IERC20 *IERC20Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IERC20.Contract.IERC20Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IERC20 *IERC20Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IERC20.Contract.IERC20Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IERC20 *IERC20Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IERC20.Contract.IERC20Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IERC20 *IERC20CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IERC20.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IERC20 *IERC20TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IERC20.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IERC20 *IERC20TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IERC20.Contract.contract.Transact(opts, method, params...)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_IERC20 *IERC20Caller) BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IERC20.contract.Call(opts, &out, "balanceOf", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_IERC20 *IERC20Session) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _IERC20.Contract.BalanceOf(&_IERC20.CallOpts, arg0)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_IERC20 *IERC20CallerSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _IERC20.Contract.BalanceOf(&_IERC20.CallOpts, arg0)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address , uint256 ) returns(bool)
func (_IERC20 *IERC20Transactor) Approve(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "approve", arg0, arg1)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address , uint256 ) returns(bool)
func (_IERC20 *IERC20Session) Approve(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Approve(&_IERC20.TransactOpts, arg0, arg1)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address , uint256 ) returns(bool)
func (_IERC20 *IERC20TransactorSession) Approve(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Approve(&_IERC20.TransactOpts, arg0, arg1)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 ) returns()
func (_IERC20 *IERC20Transactor) Burn(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "burn", arg0)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 ) returns()
func (_IERC20 *IERC20Session) Burn(arg0 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Burn(&_IERC20.TransactOpts, arg0)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 ) returns()
func (_IERC20 *IERC20TransactorSession) Burn(arg0 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Burn(&_IERC20.TransactOpts, arg0)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address , uint256 ) returns(bool)
func (_IERC20 *IERC20Transactor) Transfer(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "transfer", arg0, arg1)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address , uint256 ) returns(bool)
func (_IERC20 *IERC20Session) Transfer(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Transfer(&_IERC20.TransactOpts, arg0, arg1)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address , uint256 ) returns(bool)
func (_IERC20 *IERC20TransactorSession) Transfer(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Transfer(&_IERC20.TransactOpts, arg0, arg1)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address , address , uint256 ) returns(bool)
func (_IERC20 *IERC20Transactor) TransferFrom(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "transferFrom", arg0, arg1, arg2)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address , address , uint256 ) returns(bool)
func (_IERC20 *IERC20Session) TransferFrom(arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.TransferFrom(&_IERC20.TransactOpts, arg0, arg1, arg2)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address , address , uint256 ) returns(bool)
func (_IERC20 *IERC20TransactorSession) TransferFrom(arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.TransferFrom(&_IERC20.TransactOpts, arg0, arg1, arg2)
}

// IROUTERABI is the input ABI used to generate the binding from.
const IROUTERABI = "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// IROUTERFuncSigs maps the 4-byte function signature to its string representation.
var IROUTERFuncSigs = map[string]string{
	"1fece7b4": "deposit(address,address,uint256,string)",
}

// IROUTER is an auto generated Go binding around an Ethereum contract.
type IROUTER struct {
	IROUTERCaller     // Read-only binding to the contract
	IROUTERTransactor // Write-only binding to the contract
	IROUTERFilterer   // Log filterer for contract events
}

// IROUTERCaller is an auto generated read-only Go binding around an Ethereum contract.
type IROUTERCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IROUTERTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IROUTERTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IROUTERFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IROUTERFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IROUTERSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IROUTERSession struct {
	Contract     *IROUTER          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IROUTERCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IROUTERCallerSession struct {
	Contract *IROUTERCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// IROUTERTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IROUTERTransactorSession struct {
	Contract     *IROUTERTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// IROUTERRaw is an auto generated low-level Go binding around an Ethereum contract.
type IROUTERRaw struct {
	Contract *IROUTER // Generic contract binding to access the raw methods on
}

// IROUTERCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IROUTERCallerRaw struct {
	Contract *IROUTERCaller // Generic read-only contract binding to access the raw methods on
}

// IROUTERTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IROUTERTransactorRaw struct {
	Contract *IROUTERTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIROUTER creates a new instance of IROUTER, bound to a specific deployed contract.
func NewIROUTER(address common.Address, backend bind.ContractBackend) (*IROUTER, error) {
	contract, err := bindIROUTER(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IROUTER{IROUTERCaller: IROUTERCaller{contract: contract}, IROUTERTransactor: IROUTERTransactor{contract: contract}, IROUTERFilterer: IROUTERFilterer{contract: contract}}, nil
}

// NewIROUTERCaller creates a new read-only instance of IROUTER, bound to a specific deployed contract.
func NewIROUTERCaller(address common.Address, caller bind.ContractCaller) (*IROUTERCaller, error) {
	contract, err := bindIROUTER(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IROUTERCaller{contract: contract}, nil
}

// NewIROUTERTransactor creates a new write-only instance of IROUTER, bound to a specific deployed contract.
func NewIROUTERTransactor(address common.Address, transactor bind.ContractTransactor) (*IROUTERTransactor, error) {
	contract, err := bindIROUTER(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IROUTERTransactor{contract: contract}, nil
}

// NewIROUTERFilterer creates a new log filterer instance of IROUTER, bound to a specific deployed contract.
func NewIROUTERFilterer(address common.Address, filterer bind.ContractFilterer) (*IROUTERFilterer, error) {
	contract, err := bindIROUTER(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IROUTERFilterer{contract: contract}, nil
}

// bindIROUTER binds a generic wrapper to an already deployed contract.
func bindIROUTER(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IROUTERABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IROUTER *IROUTERRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IROUTER.Contract.IROUTERCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IROUTER *IROUTERRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IROUTER.Contract.IROUTERTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IROUTER *IROUTERRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IROUTER.Contract.IROUTERTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IROUTER *IROUTERCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IROUTER.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IROUTER *IROUTERTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IROUTER.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IROUTER *IROUTERTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IROUTER.Contract.contract.Transact(opts, method, params...)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address , address , uint256 , string ) returns()
func (_IROUTER *IROUTERTransactor) Deposit(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 string) (*types.Transaction, error) {
	return _IROUTER.contract.Transact(opts, "deposit", arg0, arg1, arg2, arg3)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address , address , uint256 , string ) returns()
func (_IROUTER *IROUTERSession) Deposit(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 string) (*types.Transaction, error) {
	return _IROUTER.Contract.Deposit(&_IROUTER.TransactOpts, arg0, arg1, arg2, arg3)
}

// Deposit is a paid mutator transaction binding the contract method 0x1fece7b4.
//
// Solidity: function deposit(address , address , uint256 , string ) returns()
func (_IROUTER *IROUTERTransactorSession) Deposit(arg0 common.Address, arg1 common.Address, arg2 *big.Int, arg3 string) (*types.Transaction, error) {
	return _IROUTER.Contract.Deposit(&_IROUTER.TransactOpts, arg0, arg1, arg2, arg3)
}

// IRUNEABI is the input ABI used to generate the binding from.
const IRUNEABI = "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"transferTo\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// IRUNEFuncSigs maps the 4-byte function signature to its string representation.
var IRUNEFuncSigs = map[string]string{
	"2ccb1b30": "transferTo(address,uint256)",
}

// IRUNE is an auto generated Go binding around an Ethereum contract.
type IRUNE struct {
	IRUNECaller     // Read-only binding to the contract
	IRUNETransactor // Write-only binding to the contract
	IRUNEFilterer   // Log filterer for contract events
}

// IRUNECaller is an auto generated read-only Go binding around an Ethereum contract.
type IRUNECaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRUNETransactor is an auto generated write-only Go binding around an Ethereum contract.
type IRUNETransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRUNEFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IRUNEFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRUNESession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IRUNESession struct {
	Contract     *IRUNE            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IRUNECallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IRUNECallerSession struct {
	Contract *IRUNECaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IRUNETransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IRUNETransactorSession struct {
	Contract     *IRUNETransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IRUNERaw is an auto generated low-level Go binding around an Ethereum contract.
type IRUNERaw struct {
	Contract *IRUNE // Generic contract binding to access the raw methods on
}

// IRUNECallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IRUNECallerRaw struct {
	Contract *IRUNECaller // Generic read-only contract binding to access the raw methods on
}

// IRUNETransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IRUNETransactorRaw struct {
	Contract *IRUNETransactor // Generic write-only contract binding to access the raw methods on
}

// NewIRUNE creates a new instance of IRUNE, bound to a specific deployed contract.
func NewIRUNE(address common.Address, backend bind.ContractBackend) (*IRUNE, error) {
	contract, err := bindIRUNE(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IRUNE{IRUNECaller: IRUNECaller{contract: contract}, IRUNETransactor: IRUNETransactor{contract: contract}, IRUNEFilterer: IRUNEFilterer{contract: contract}}, nil
}

// NewIRUNECaller creates a new read-only instance of IRUNE, bound to a specific deployed contract.
func NewIRUNECaller(address common.Address, caller bind.ContractCaller) (*IRUNECaller, error) {
	contract, err := bindIRUNE(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IRUNECaller{contract: contract}, nil
}

// NewIRUNETransactor creates a new write-only instance of IRUNE, bound to a specific deployed contract.
func NewIRUNETransactor(address common.Address, transactor bind.ContractTransactor) (*IRUNETransactor, error) {
	contract, err := bindIRUNE(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IRUNETransactor{contract: contract}, nil
}

// NewIRUNEFilterer creates a new log filterer instance of IRUNE, bound to a specific deployed contract.
func NewIRUNEFilterer(address common.Address, filterer bind.ContractFilterer) (*IRUNEFilterer, error) {
	contract, err := bindIRUNE(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IRUNEFilterer{contract: contract}, nil
}

// bindIRUNE binds a generic wrapper to an already deployed contract.
func bindIRUNE(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IRUNEABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRUNE *IRUNERaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRUNE.Contract.IRUNECaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRUNE *IRUNERaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRUNE.Contract.IRUNETransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRUNE *IRUNERaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRUNE.Contract.IRUNETransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRUNE *IRUNECallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRUNE.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRUNE *IRUNETransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRUNE.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRUNE *IRUNETransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRUNE.Contract.contract.Transact(opts, method, params...)
}

// TransferTo is a paid mutator transaction binding the contract method 0x2ccb1b30.
//
// Solidity: function transferTo(address , uint256 ) returns(bool)
func (_IRUNE *IRUNETransactor) TransferTo(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRUNE.contract.Transact(opts, "transferTo", arg0, arg1)
}

// TransferTo is a paid mutator transaction binding the contract method 0x2ccb1b30.
//
// Solidity: function transferTo(address , uint256 ) returns(bool)
func (_IRUNE *IRUNESession) TransferTo(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRUNE.Contract.TransferTo(&_IRUNE.TransactOpts, arg0, arg1)
}

// TransferTo is a paid mutator transaction binding the contract method 0x2ccb1b30.
//
// Solidity: function transferTo(address , uint256 ) returns(bool)
func (_IRUNE *IRUNETransactorSession) TransferTo(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRUNE.Contract.TransferTo(&_IRUNE.TransactOpts, arg0, arg1)
}
