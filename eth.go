package main

import (
	"crypto/ecdsa"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	ecommon "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

var (
	// ETH settings
	ethRPCHost string
)

const ETHToken = `0x0000000000000000000000000000000000000000`

func getETHClient() (*ethclient.Client, error) {
	if ethRPCHost == "" {
		return nil, fmt.Errorf("eth rpc host is empty")
	}
	return ethclient.Dial(ethRPCHost)
}
func getETHAddressBalance(ctx *cli.Context, address, contract, token string) (uint64, error) {
	if len(address) == 0 {
		return 0, errors.New("please provide address")
	}
	if ctx.Bool("verbose") {
		fmt.Println("get ETH address balance:", address)
	}
	client, err := getETHClient()
	if err != nil {
		return 0, fmt.Errorf("fail to get ETH client: %w", err)
	}
	if token == ETHToken {
		resp, err := client.BalanceAt(ctx.Context, ecommon.HexToAddress(address), nil)
		if err != nil {
			return 0.0, fmt.Errorf("fail to get the balance of address: %s, %w", address, err)
		}
		// convert ETH balance from 1e18 -> 1e8
		return big.NewInt(0).Div(resp, big.NewInt(1e10)).Uint64(), nil
	}
	router, err := NewRouter(ecommon.HexToAddress(contract), client)
	if err != nil {
		return 0, fmt.Errorf("fail to create router object: %w", err)
	}

	v, err := router.VaultAllowance(nil, ecommon.HexToAddress(address), ecommon.HexToAddress(token))
	if err != nil {
		return 0, fmt.Errorf("fail to get vault allowance: %w", err)
	}
	// convert ETH balance from 1e18 -> 1e8
	if strings.EqualFold(token, "0X62E273709DA575835C7F6AEF4A31140CA5B1D190") ||
		strings.EqualFold(token, "0XDAC17F958D2EE523A2206206994597C13D831EC7") ||
		strings.EqualFold(token, "0XA3910454BF2CB59B8B3A401589A3BACC5CA42306") ||
		strings.EqualFold(token, "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48") {
		return big.NewInt(0).Mul(v, big.NewInt(100)).Uint64(), nil
	}
	if strings.EqualFold(token, "0X242AD49DACD38AC23CAF2CCC118482714206BED4") {
		return v.Uint64(), nil
	}
	if strings.EqualFold(token, "0XA0B515C058F127A15DD3326F490EBF47D215588E") {
		return big.NewInt(0).Div(v, big.NewInt(100)).Uint64(), nil
	}

	return big.NewInt(0).Div(v, big.NewInt(1e10)).Uint64(), nil
}

func getETHAddressRawBalance(ctx *cli.Context, address, contract, token string) (*big.Int, error) {
	if len(address) == 0 {
		return nil, errors.New("please provide address")
	}
	if ctx.Bool("verbose") {
		fmt.Println("get ETH address balance:", address)
	}
	client, err := getETHClient()
	if err != nil {
		return nil, fmt.Errorf("fail to get ETH client: %w", err)
	}
	if token == ETHToken {
		return client.BalanceAt(ctx.Context, ecommon.HexToAddress(address), nil)
	}
	router, err := NewRouter(ecommon.HexToAddress(contract), client)
	if err != nil {
		return nil, fmt.Errorf("fail to create router object: %w", err)
	}

	return router.VaultAllowance(nil, ecommon.HexToAddress(address), ecommon.HexToAddress(token))
}

func sendETHAction(ctx *cli.Context, vault types.Vault, gasRate int64, asgardAddress, memo string) error {
	var ethContract types.ChainContract
	for _, r := range vault.Routers {
		if r.Chain.Equals(common.ETHChain) {
			ethContract = r
			break
		}
	}
	ethAddr, err := vault.PubKey.GetAddress(common.ETHChain)
	if err != nil {
		return fmt.Errorf("fail to get ETH address: %w", err)
	}
	if ethContract.IsEmpty() {
		return fmt.Errorf("fail to get eth contract")
	}
	var coins []RouterCoin
	ethValue := big.NewInt(0)

	for _, item := range vault.Coins {
		if !item.Asset.Chain.Equals(common.ETHChain) {
			continue
		}
		if item.Asset.Equals(common.ETHAsset) {
			ethBalance, err := getETHAddressRawBalance(ctx, ethAddr.String(), ethContract.Router.String(), ETHToken)
			if err != nil {
				return fmt.Errorf("fail to get ETH balance,err : %w", err)
			}
			ethValue = ethBalance
			continue
		}
		parts := strings.Split(item.Asset.Symbol.String(), "-")
		balance, err := getETHAddressRawBalance(ctx, ethAddr.String(), ethContract.Router.String(), parts[1])
		if err != nil {
			return fmt.Errorf("fail to get ETH balance: %w", err)
		}
		coins = append(coins, RouterCoin{
			Asset:  ecommon.HexToAddress(parts[1]),
			Amount: balance,
		})
	}

	ethClient, err := getETHClient()
	if err != nil {
		return fmt.Errorf("fail to get ETH client,err: %w", err)
	}

	router, err := NewRouter(ecommon.HexToAddress(ethContract.Router.String()), ethClient)
	if err != nil {
		return fmt.Errorf("fail to create router instance")
	}
	gasPrice := big.NewInt(1).Mul(big.NewInt(gasRate), big.NewInt(common.One*10))
	ethValue = ethValue.Sub(ethValue, big.NewInt(1).Mul(gasPrice, big.NewInt(120000)))
	if ethValue.Int64() <= 0 {
		ethValue = ethValue.SetInt64(0)
	}
	auth, err := getTransactOpts(ctx, ethClient, privateKey, ethValue, gasPrice)
	if err != nil {
		return fmt.Errorf("fail to get transact opts,err:%w", err)
	}
	auth.GasLimit = 120000
	tx, err := router.ReturnVaultAssets(auth, ecommon.HexToAddress(ethContract.Router.String()), ecommon.HexToAddress(asgardAddress), coins, memo)
	if err != nil {
		return fmt.Errorf("fail to send ETH yggdrasil return tx,err: %w", err)
	}
	fmt.Println("ETH Transaction:", tx.Hash().String())
	return nil
}

func getTransactOpts(ctx *cli.Context, client *ethclient.Client, key string, ethValue *big.Int, gasPrice *big.Int) (*bind.TransactOpts, error) {
	privateKey, err := crypto.HexToECDSA(key)
	if err != nil {
		return nil, fmt.Errorf("fail to get private key: %w", err)
	}
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("fail to public key to ECDSA")
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(ctx.Context, fromAddress)
	if err != nil {
		return nil, fmt.Errorf("fail to get pending nonce for address: %s,%w", fromAddress, err)
	}

	chainID, err := client.ChainID(ctx.Context)
	if err != nil {
		return nil, fmt.Errorf("fail to get chain id : %w", err)
	}
	// 4 is rinkeby
	// 1337 is used by dev
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, chainID)
	if err != nil {
		return nil, fmt.Errorf("fail to get tx options")
	}

	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = ethValue
	auth.GasPrice = gasPrice
	return auth, nil
}
