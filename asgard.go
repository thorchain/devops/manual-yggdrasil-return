package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

type address struct {
	Chain   common.Chain   `json:"chain,omitempty"`
	PubKey  common.PubKey  `json:"pub_key,omitempty"`
	Address common.Address `json:"address,omitempty"`
	Router  common.Address `json:"router,omitempty"`
	Halted  bool           `json:"halted"`
	GasRate cosmos.Uint    `json:"gas_rate,omitempty"`
}

func getInboundAddresses(ctx *cli.Context, serverIP string, port int) ([]address, error) {
	result, err := getFromThorchain(serverIP, port, "inbound_addresses")
	if err != nil {
		return nil, fmt.Errorf("fail to get inbound addresses")
	}
	var addresses []address
	if err := json.Unmarshal(result, &addresses); err != nil {
		return nil, fmt.Errorf("fail to unmarshal addresses, err: %w", err)
	}
	return addresses, nil
}
func getOutbound(ctx *cli.Context, serverIP string, port int, nodePubKey string) (string, error) {
	result, err := getFromThorchain(serverIP, port, "queue/outbound")
	if err != nil {
		return "", fmt.Errorf("fail to get inbound addresses")
	}
	var outbound []types.TxOutItem
	if err := json.Unmarshal(result, &outbound); err != nil {
		return "", fmt.Errorf("fail to unmarshal outbound tx,err:%w", err)
	}
	for _, item := range outbound {
		if strings.EqualFold(item.VaultPubKey.String(), nodePubKey) {
			return item.Memo, nil
		}
	}
	return "", fmt.Errorf("didn't find memo")
}
