package main

import (
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/olekukonko/tablewriter"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
)

var (
	privateKey string
)

func setupConfig() {
	// Read in the configuration file for the sdk
	nw := common.GetCurrentChainNetwork()
	switch nw {
	case common.TestNet, common.MockNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tthor", "tthorpub")
		config.SetBech32PrefixForValidator("tthorv", "tthorvpub")
		config.SetBech32PrefixForConsensusNode("tthorc", "tthorcpub")
		config.Seal()
	case common.MainNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("thor", "thorpub")
		config.SetBech32PrefixForValidator("thorv", "thorvpub")
		config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")
		config.Seal()
	}
}

func main() {
	setupConfig()
	app := &cli.App{
		Name:  "m-yggdrasilreturn",
		Usage: "this tool is to allow node operators to return yggdrasil fund manually",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "server",
				Aliases:    []string{"s"},
				Usage:      "thornode server url or ip address",
				EnvVars:    []string{"THORNODE_IP"},
				Value:      "localhost",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
			&cli.IntFlag{
				Name:       "port",
				Aliases:    []string{"p"},
				Usage:      "port",
				EnvVars:    []string{"SERVER_PORT"},
				Value:      1317,
				HasBeenSet: true,
			},
			&cli.StringFlag{
				Name:     "binance",
				Aliases:  []string{"b"},
				Usage:    "binance server url host:port",
				EnvVars:  []string{"BINANCE_URL"},
				Value:    "https://dataseed5.defibit.io/",
				Required: false,
				Hidden:   false,
			},

			&cli.StringFlag{
				Name:     "btc-host",
				Usage:    "bitcoind node rpc host",
				Required: true,
				Hidden:   false,
				Value:    "127.0.0.1:18443",
				EnvVars: []string{
					"BTC_HOST",
				},
				Destination: &btcHost,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "btc-username",
				Usage:       "username used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "thorchain",
				Destination: &btcRpcUserName,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "btc-password",
				Usage:       "password used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "password",
				Destination: &btcRpcUserPassword,
				HasBeenSet:  true,
			},

			&cli.StringFlag{
				Name:        "btc-network",
				Usage:       "bitcoin network, [mainnet,testnet,regtest]",
				Required:    true,
				Hidden:      false,
				Value:       "testnet3",
				DefaultText: "",
				Destination: nil,
				HasBeenSet:  true,
			},

			&cli.StringFlag{
				Name:        "bch-host",
				Usage:       "bitcoind node rpc host",
				Required:    true,
				Hidden:      false,
				Value:       "127.0.0.1:28443",
				EnvVars:     []string{"BCH_HOST"},
				Destination: &bchHost,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "bch-username",
				Usage:       "username used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "thorchain",
				Destination: &bchRpcUserName,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "bch-password",
				Usage:       "password used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "password",
				Destination: &bchRpcUserPassword,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "bch-network",
				Usage:       "bitcoin network, [mainnet,testnet,regtest]",
				Required:    true,
				Hidden:      false,
				Value:       "testnet3",
				DefaultText: "",
				Destination: nil,
				HasBeenSet:  true,
			},

			&cli.StringFlag{
				Name:     "ltc-host",
				Usage:    "bitcoind node rpc host",
				Required: true,
				Hidden:   false,
				Value:    "127.0.0.1:38443",
				EnvVars: []string{
					"LTC_HOST",
				},
				Destination: &ltcHost,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "ltc-username",
				Usage:       "username used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "thorchain",
				Destination: &ltcRpcUserName,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "ltc-password",
				Usage:       "password used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "password",
				Destination: &ltcRpcUserPassword,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "ltc-network",
				Usage:       "litecoin network, [mainnet,testnet,regtest]",
				Required:    true,
				Hidden:      false,
				Value:       "testnet3",
				DefaultText: "",
				Destination: nil,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "eth-host",
				Usage:       "ETH node rpc host",
				Required:    true,
				Hidden:      false,
				Value:       "http://127.0.0.1:8545",
				Destination: &ethRPCHost,
				HasBeenSet:  true,
				EnvVars: []string{
					"ETH_HOST",
				},
			},
			&cli.StringFlag{
				Name:        "key",
				Usage:       "hex encoded private key",
				Required:    true,
				Hidden:      false,
				Destination: &privateKey,
				HasBeenSet:  false,
				EnvVars: []string{
					"PRIVATE_KEY",
				},
			},
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: nil,
				Usage:   "verbose mode , print out more information",
				Value:   false,
			},
		},
		Action: yggdrasilReturnAction,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func yggdrasilReturnAction(ctx *cli.Context) error {
	if len(privateKey) == 0 {
		return fmt.Errorf("please provide yggdrasil hex encoded private key using --key parameter")
	}
	privKeyBytes, err := hex.DecodeString(privateKey)
	if err != nil {
		return fmt.Errorf("fail to decode private key, err:%w", err)
	}
	privKey := secp256k1.PrivKey(privKeyBytes)
	pubKey := privKey.PubKey()

	publicKey, err := common.NewPubKeyFromCrypto(pubKey)
	if err != nil {
		return fmt.Errorf("fail to get public key, err: %w", err)
	}
	thorAddr, err := publicKey.GetAddress(common.THORChain)
	if err != nil {
		return fmt.Errorf("fail to get thor address: %w", err)
	}
	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Is %s your node address?", thorAddr.String()),
		IsConfirm: true,
	}
	result, err := prompt.Run()
	if err != nil {
		return nil
	}
	if result != "y" {
		return nil
	}
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("please provide THORNODE server ip address usign --server")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	binanceRPC := ctx.String("binance")
	if binanceRPC == "" {
		return errors.New("please provide binance RPC server address using --binance")
	}
	fmt.Println("getting yggdrasil vault ...")
	yggdrasilVault, err := getYggdrasilByPubKey(serverIP, port, publicKey.String())
	if err != nil {
		return fmt.Errorf("fail to get yggdrasil vault from thornode,err: %w", err)
	}
	bnbAddr, err := yggdrasilVault.PubKey.GetAddress(common.BNBChain)
	if err != nil {
		return fmt.Errorf("fail to get binance address,err:%w", err)
	}
	account, err := GetAccountByAddress(binanceRPC, bnbAddr.String())
	if err != nil {
		return fmt.Errorf("fail to get account from binance(%s),err: %w", binanceRPC, err)
	}
	if err := printYggdrasilVault(ctx, yggdrasilVault, account); err != nil {
		return err
	}
	fmt.Println("getting current active asgard addresses...")
	addresses, err := getInboundAddresses(ctx, serverIP, port)
	if err != nil {
		return fmt.Errorf("fail to get current inbound addresses: %w", err)
	}
	w := tablewriter.NewWriter(os.Stdout)
	w.SetHeader([]string{
		"chain",
		"address",
		"gas",
	})
	for _, item := range addresses {
		w.Append([]string{
			item.Chain.String(),
			item.Address.String(),
			item.GasRate.String(),
		})
	}
	w.Render()
	prompt = promptui.Prompt{
		Label:     "Is this the correct asgard address",
		IsConfirm: true,
	}
	result, err = prompt.Run()
	if err != nil {
		return err
	}
	if result != "y" {
		return nil
	}
	memo, err := getOutbound(ctx, serverIP, port, publicKey.String())
	if err != nil {
		fmt.Printf("fail to get outbound from thornode,err: %s \n", err)
	}
	if len(memo) == 0 {
		prompt = promptui.Prompt{
			Label: "provide yggdrasil return memo , usually something like yggdrasil-:{blockheight}",
		}
		memo, err = prompt.Run()
		if err != nil {
			return fmt.Errorf("fail to get yggdrasil return memo,err: %w", err)
		}
	}
	prompt = promptui.Prompt{
		Label:     fmt.Sprintf("use %s as memo", memo),
		IsConfirm: true,
	}
	result, err = prompt.Run()
	if err != nil {
		return nil
	}
	if result != "y" {
		return nil
	}

	bnbAddrInfo := getAddress(addresses, common.BNBChain)
	if bnbAddrInfo.Address.IsEmpty() {
		return fmt.Errorf("fail to get bnb address")
	}
	fmt.Printf("sending BNB transaction to %s \n", bnbAddrInfo.Address.String())
	if err := sendBinanceTransaction(ctx, binanceRPC, privateKey, bnbAddrInfo.Address.String(), memo, account, bnbAddr.String()); err != nil {
		return fmt.Errorf("fail to send BNB transaction, err: %w", err)
	}

	btcAddr := getAddress(addresses, common.BTCChain)
	if btcAddr.Address.IsEmpty() {
		return fmt.Errorf("fail to get btc address")
	}
	fmt.Printf("sending BTC transaction to %s \n", btcAddr.Address.String())
	if err := sendBitcoinAction(ctx, privateKey, btcAddr.Address.String(), int64(btcAddr.GasRate.Uint64()), memo); err != nil {
		return fmt.Errorf("fail to send btc transaction, err: %w", err)
	}

	ltcAddr := getAddress(addresses, common.LTCChain)
	if ltcAddr.Address.IsEmpty() {
		return fmt.Errorf("fail to get LTC address")
	}
	fmt.Printf("sending LTC transaction to %s \n", ltcAddr.Address.String())
	if err := sendLitecoinAction(ctx, privateKey, ltcAddr.Address.String(), int64(ltcAddr.GasRate.Uint64()), memo); err != nil {
		return fmt.Errorf("fail to send LTC transaction,err: %w", err)
	}

	bchAddr := getAddress(addresses, common.BCHChain)
	if bchAddr.Address.IsEmpty() {
		return fmt.Errorf("fail to get BCH address")
	}
	fmt.Printf("sending BCH transaction to %s \n", bchAddr.Address.String())
	if err := sendBitcoinCashAction(ctx, privateKey, bchAddr.Address.String(), int64(bchAddr.GasRate.Uint64()), memo); err != nil {
		return fmt.Errorf("fail to send BCH transaction,err: %w", err)
	}
	ethAddr := getAddress(addresses, common.ETHChain)
	if ethAddr.Address.IsEmpty() {
		return fmt.Errorf("fail to get ETH address")
	}
	fmt.Printf("sending ETH transaction to: %s, router contract: %s \n", ethAddr.Address, ethAddr.Router)
	if err := sendETHAction(ctx, yggdrasilVault, int64(ethAddr.GasRate.Uint64()), ethAddr.Address.String(), memo); err != nil {
		return fmt.Errorf("fail to send ETH transaction,err: %w", err)
	}
	return nil
}
func getAddress(addresses []address, chain common.Chain) address {
	for _, item := range addresses {
		if item.Chain.Equals(chain) {
			return item
		}
	}
	return address{}
}
